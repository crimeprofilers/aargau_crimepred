
def G(v):
    bins = np.linspace(0., 100., 51)
    total = float(np.sum(v))
    yvals = []
    ## ordered from highest value to lowest value
    ## this is different than classic Lorenz curve!
    for b in bins:
        bin_vals = v[v >= np.percentile(v, 100-b)]
        bin_fraction = (np.sum(bin_vals) / total) * 100.0
        yvals.append(bin_fraction)
    # perfect equality area
    pe_area = np.trapz(bins, x=bins)
    # lorenz area
    lorenz_area = np.trapz(yvals, x=bins)
    return bins, yvals

bins, result_incidents_2015 = G(ct_orig_2015['incidents_2015'])
bins, result_incidents_2014 = G(ct_orig_2014['incidents_2014'])

bins, result_larcenies_2015 = G(ct_orig_2015['larcenies_2015'])
bins, result_larcenies_2014 = G(ct_orig_2014['larcenies_2014'])

bins, result_assaults_2015 = G(ct_orig_2015['assaults_2015'])
bins, result_assaults_2014 = G(ct_orig_2014['assaults_2014'])


plt.figure()
plt.plot(bins, result_larcenies_2015, 's-', color = 'blue', label = 'larcenies 2015', alpha = 0.65)
plt.plot(bins, result_larcenies_2014, 's-', color = 'blue', label = 'larcenies 2014', alpha = 0.25)

plt.plot(bins, result_assaults_2015, 'd-', color = 'red', label = 'assaults 2015', alpha = 0.65)
plt.plot(bins, result_assaults_2014, 'd-', color = 'red', label = 'assaults 2014', alpha = 0.25)

plt.plot(bins, result_incidents_2015, 'o-', color = 'black', label = 'total incidents 2015', alpha = 0.65)
plt.plot(bins, result_incidents_2014, 'o-', color = 'black', label = 'total incidents 2014', alpha = 0.25)

plt.plot(bins, bins, '-.', color ='black')
plt.xlabel("Cummulative % census tracts")
plt.ylabel("cummulative % crimes")
plt.xticks([0,10,20,30,40,50,60,70,80,90,100])
plt.yticks([0,10,20,30,40,50,60,70,80,90,100])
#plt.axis('equal')
plt.gca().set_aspect('equal', adjustable='box')
plt.xlim(0,100)
plt.ylim(0,100)
plt.legend(loc='lower right')
plt.tight_layout()
plt.show()
plt.clf()
