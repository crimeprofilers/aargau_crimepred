import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots
import seaborn

# read data
data = pd.read_csv('vis_data2.csv')

# found a bug: needed to return 100-bin_fraction
print(float(np.sum(data['count'])))
def G(v):
    bins = np.linspace(0., 100., 101)
    total = float(np.sum(v))
    yvals = []
    ## ordered from highest value to lowest value
    ## this is different than classic Lorenz curve!
    for b in bins:
        bin_vals = v[v <= np.percentile(v, 100-b)]
        bin_fraction = (np.sum(bin_vals) / total) * 100.0
        yvals.append(100-bin_fraction)
    # perfect equality area
    pe_area = np.trapz(bins, x=bins)
    # lorenz area
    lorenz_area = np.trapz(yvals, x=bins)
    return bins, yvals

bins, burglaries = G(data['count'])


fig, ax = subplots()

#ax.spines['top'].set_visible(True)
#ax.spines['right'].set_visible(True)
ax.spines['left'].set_visible(True)
ax.spines['left'].set_color("black")

#ax.set_axis_on()
#ax.set_frame_on(True)
#ax.set_aspect('equal')



plt.cla()
plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.3)
plt.box(on=True)

plt.plot(bins, burglaries, '-', color = 'black', label = 'Burglaries')
plt.plot(bins, bins, '.', color ='black')

#plt.gca().spines['bottom'].set_color('black')
#plt.gca().spines['left'].set_color('black')
#plt.gca().spines['right'].set_color('black')


plt.xlabel("Cummulative % Cells", fontsize=12)
plt.ylabel("Cummulative % Crimes", fontsize=12)
plt.xticks([0,10,20,30,40,50,60,70,80,90,100])
plt.yticks([0,10,20,30,40,50,60,70,80,90,100])

#plt.xlim(0,105)
#plt.ylim(0,105)
plt.legend(loc='lower right')
plt.tight_layout()
#plt.clf()



ax.set_aspect('equal')
ax.set_axis_bgcolor('white')

#ax.set_axis_on()
#ax.set_frame_on(True)

#ax.set_facecolor('black')
#ax.set_fc('black')

#https://matplotlib.org/api/axes_api.html#appearance
#plt.show()