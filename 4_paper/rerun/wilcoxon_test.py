import numpy as np
import pandas as pd
import seaborn as sns
from scipy.stats import wilcoxon

pred_cov_list = np.arange(0.01,1.01,0.01)

wilcoxon_list = []

for pred_cov in pred_cov_list:

	ml = np.genfromtxt(str("/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/PAI/PAI_list_ml_cov" + str(pred_cov) + str(".csv")))
	hs = np.genfromtxt(str("/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/PAI/PAI_HS_cov") + str(pred_cov) + str(".csv"))

	z_statistic, p_value = wilcoxon(hs, ml)
	wilcoxon_list.append([pred_cov,z_statistic, p_value])

	print pred_cov
	print '  ', z_statistic, p_value

np.savetxt("/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/PAI/wilcoxon.csv",wilcoxon_list,delimiter=',')