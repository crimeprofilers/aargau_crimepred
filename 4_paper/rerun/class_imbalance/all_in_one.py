import numpy as np

import pandas as pd

import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt

from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import cross_val_score, train_test_split
from sklearn.metrics import roc_auc_score, roc_curve, auc, confusion_matrix, fbeta_score, make_scorer
from sklearn.grid_search import GridSearchCV

from imblearn.over_sampling import RandomOverSampler, SMOTE
from imblearn.ensemble import EasyEnsemble
from imblearn.under_sampling import NearMiss, RandomUnderSampler

def load_data(euler):

	print '...importing data...'
	if euler == 0:
		data = pd.read_csv('/Users/rudimac/Documents/MTEC/THESIS/FEATURES/dataset_final.csv', index_col=[1,2]).drop(['Unnamed: 0'],axis=1)
	elif euler == 1:
		data = pd.read_csv('/cluster/home/rudolfm/csv/dataset_final.csv', index_col=[1,2]).drop(['Unnamed: 0'],axis=1)
	else:
		print 'Specify data path!'

	print '...splitting into features and target...'
	features = data.drop(['offence','probability','offence_ids','pred_class','probability_norm','pred_class_norm','pred_class_bin','to_date'],axis=1)
	target = data['to_date']
	target = target.astype(int)

	print '\n\nFEATURE SHAPE: ',features.shape,'TARGET SHAPE: ', target.shape

	feat_names = features.columns

	print '\nTotal Number of cells: ', features.loc['2014-01-14',:].shape[0]
	print '\nTotal Number of days: ', features.loc[(slice('2014-01-14','2017-01-13'),1429),:].shape[0]
	print 'Number of training days: ', features.loc[(slice('2014-01-14','2016-01-13'),1429),:].shape[0]
	print 'Number of testing days: ', features.loc[(slice('2016-01-14','2017-01-13'),1429),:].shape[0]

	print '\nClass distributions - Original Dataset'
	print 'Class 0: ', target.value_counts()[0], '(',float(target.value_counts()[0])/target.shape[0]*100, '% )'
	print 'Class 1: ', target.value_counts()[1], '(',float(target.value_counts()[1])/target.shape[0]*100, '% )'

	# split dataset into training and testing data
	X_train = features.loc['2014-01-14':'2016-01-13',:].values
	y_train = target.loc['2014-01-14':'2016-01-13',:].values
	X_test = features.loc['2016-01-14':'2017-01-13',:].values
	y_test = target.loc['2016-01-14':'2017-01-13',:].values

	tot = len(y_train)
	print '\nClass distributions - Training Dataset'
	print 'Class 0: ', np.count_nonzero(y_train == 0), '(',float(np.count_nonzero(y_train == 0))/tot*100, '% )'
	print 'Class 1: ', np.count_nonzero(y_train == 1), '(',float(np.count_nonzero(y_train == 1))/tot*100, '% )'

	tot = len(y_test)
	print '\nClass distributions - Testing Dataset'
	print 'Class 0: ', np.count_nonzero(y_test == 0), '(',float(np.count_nonzero(y_test == 0))/tot*100, '% )'
	print 'Class 1: ', np.count_nonzero(y_test == 1), '(',float(np.count_nonzero(y_test == 1))/tot*100, '% )'

	return X_train,y_train,X_test,y_test,feat_names

def oversample_random(X, y):

	print '\n...oversampling...'
	ros = RandomOverSampler()
	X_res, y_res = ros.fit_sample(X, y)

	print '\nOversampled Dataset: '
	print 'Class 0: ', np.count_nonzero(y_res == 0)
	print 'Class 1: ', np.count_nonzero(y_res == 1)

	return X_res, y_res

def oversample_SMOTE(X, y):

	print '\n...oversampling...'
	sm = SMOTE(k_neighbors=3, n_jobs=24)
	X_res, y_res = sm.fit_sample(X, y)

	print '\nOversampled Dataset: '
	print 'Class 0: ', np.count_nonzero(y_res == 0)
	print 'Class 1: ', np.count_nonzero(y_res == 1)

	return X_res, y_res

def undersamp_easy_ensemble(n_subsets, X_train, y_train):

	print '\n...resampling...'
	# iteratively select a random subset and make an ensemble of the different sets
	print '\nNumber of undersampling subsets: ____', n_subsets, '____\n'
	ee = EasyEnsemble(n_subsets=n_subsets, random_state=4242)
	X_res, y_res = ee.fit_sample(X_train, y_train)

	print '\nBalanced Dataset: '
	print 'Class 0: ', np.count_nonzero(y_res[0] == 0)
	print 'Class 1: ', np.count_nonzero(y_res[0] == 1)

	return X_res,y_res

def undersamp_near_miss(X_train, y_train, vers):

	print '\n...resampling...'
	nm = NearMiss(random_state=0, version=vers, n_jobs=24)
	X_res, y_res = nm.fit_sample(X_train, y_train)

	print '\nBalanced Dataset: '
	print 'Class 0: ', np.count_nonzero(y_res == 0)
	print 'Class 1: ', np.count_nonzero(y_res == 1)

	return X_res,y_res

def undersamp_random(X_train, y_train):

	print '\n...resampling...'
	# iteratively select a random subset and make an ensemble of the different sets
	rus = RandomUnderSampler(random_state=42)
	X_res, y_res = rus.fit_sample(X_train, y_train)

	print '\nBalanced Dataset: '
	print 'Class 0: ', np.count_nonzero(y_res == 0)
	print 'Class 1: ', np.count_nonzero(y_res == 1)

	return X_res,y_res

def ROC_curve_cost_sensitive(pred_class1,y_test,euler):
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
	roc_auc = auc(false_positive_rate, true_positive_rate)

	plt.title('Receiver Operating Characteristic')
	plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC = %0.2f'% roc_auc)
	plt.legend(loc='lower right')
	plt.plot([0,1],[0,1],'r--')
	plt.xlim([-0.1,1.1])
	plt.ylim([-0.1,1.1])
	plt.ylabel('True Positive Rate')
	plt.xlabel('False Positive Rate')

	if euler == 0:
		plt.savefig('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/ROC_curve_balanced_weights.png')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_balanced_weights_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_balanced_weights_TPR.csv', true_positive_rate, delimiter=',')
	elif euler == 1:
		plt.savefig('/cluster/home/rudolfm/results/ROC_curve_balanced_weights.png')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_balanced_weights_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_balanced_weights_TPR.csv', true_positive_rate, delimiter=',')

def ROC_curve_random_oversamp(pred_class1,y_test,euler):
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
	roc_auc = auc(false_positive_rate, true_positive_rate)

	plt.title('Receiver Operating Characteristic')
	plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC = %0.2f'% roc_auc)
	plt.legend(loc='lower right')
	plt.plot([0,1],[0,1],'r--')
	plt.xlim([-0.1,1.1])
	plt.ylim([-0.1,1.1])
	plt.ylabel('True Positive Rate')
	plt.xlabel('False Positive Rate')

	if euler == 0:
		plt.savefig('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/ROC_curve_oversamp_ROS.png')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_ROS_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_ROS_TPR.csv', true_positive_rate, delimiter=',')
	elif euler == 1:
		plt.savefig('/cluster/home/rudolfm/results/ROC_curve_oversamp_ROS.png')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_ROS_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_ROS_TPR.csv', true_positive_rate, delimiter=',')

def ROC_curve_SMOTE(pred_class1,y_test,euler):
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
	roc_auc = auc(false_positive_rate, true_positive_rate)

	plt.title('Receiver Operating Characteristic')
	plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC = %0.2f'% roc_auc)
	plt.legend(loc='lower right')
	plt.plot([0,1],[0,1],'r--')
	plt.xlim([-0.1,1.1])
	plt.ylim([-0.1,1.1])
	plt.ylabel('True Positive Rate')
	plt.xlabel('False Positive Rate')

	if euler == 0:
		plt.savefig('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/ROC_curve_oversamp_SMOTE.png')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_SMOTE_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_SMOTE_TPR.csv', true_positive_rate, delimiter=',')
	elif euler == 1:
		plt.savefig('/cluster/home/rudolfm/results/ROC_curve_oversamp_SMOTE.png')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_SMOTE_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_SMOTE_TPR.csv', true_positive_rate, delimiter=',')

def ROC_curve_stacked(pred_class1,y_test,euler):
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
	roc_auc = auc(false_positive_rate, true_positive_rate)

	plt.title('Receiver Operating Characteristic')
	plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC = %0.2f'% roc_auc)
	plt.legend(loc='lower right')
	plt.plot([0,1],[0,1],'r--')
	plt.xlim([-0.1,1.1])
	plt.ylim([-0.1,1.1])
	plt.ylabel('True Positive Rate')
	plt.xlabel('False Positive Rate')

	if euler == 0:
		plt.savefig('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/ROC_curve_undersamp_easy_ensemble.png')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_easy_ensemble_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_easy_ensemble_TPR.csv', true_positive_rate, delimiter=',')
	elif euler == 1:
		plt.savefig('/cluster/home/rudolfm/results/ROC_curve_undersamp_easy_ensemble.png')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_easy_ensemble_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_easy_ensemble_TPR.csv', true_positive_rate, delimiter=',')

def ROC_curve_near_miss(pred_class1,y_test,euler):
	
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
	roc_auc = auc(false_positive_rate, true_positive_rate)

	plt.title('Receiver Operating Characteristic')
	plt.plot(false_positive_rate, true_positive_rate, 'blue', label='v1 AUC = %0.2f'% roc_auc)
	plt.legend(loc='lower right')
	plt.plot([0,1],[0,1],'r--')
	plt.xlim([-0.1,1.1])
	plt.ylim([-0.1,1.1])
	plt.ylabel('True Positive Rate')
	plt.xlabel('False Positive Rate')
	
	if euler == 0:
		plt.savefig('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/ROC_curve_undersamp_nearmiss.png')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_near_miss_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_near_miss_TPR.csv', true_positive_rate, delimiter=',')
	elif euler == 1:
		plt.savefig('/cluster/home/rudolfm/results/ROC_curve_undersamp_nearmiss.png')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_near_miss_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_near_miss_TPR.csv', true_positive_rate, delimiter=',')

def ROC_curve_undersamp_random(pred_class1,y_test,euler):
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
	roc_auc = auc(false_positive_rate, true_positive_rate)

	plt.title('Receiver Operating Characteristic')
	plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC = %0.2f'% roc_auc)
	plt.legend(loc='lower right')
	plt.plot([0,1],[0,1],'r--')
	plt.xlim([-0.1,1.1])
	plt.ylim([-0.1,1.1])
	plt.ylabel('True Positive Rate')
	plt.xlabel('False Positive Rate')

	if euler == 0:
		plt.savefig('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/ROC_curve_undersamp_random.png')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_undersamp_random_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_undersamp_random_TPR.csv', true_positive_rate, delimiter=',')
	elif euler == 1:
		plt.savefig('/cluster/home/rudolfm/results/ROC_curve_undersamp_random.png')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_undersamp_random_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_undersamp_random_TPR.csv', true_positive_rate, delimiter=',')

if __name__ == '__main__':

	print '\n\n\n\n\n\n'
	
	# local = 0 or cluster = 1:
	euler = 0

	if euler == 0:
		parallel_jobs = 8

	elif euler == 1:
		parallel_jobs = 24

	# load data and split into train and test set
	X_train, y_train, X_test, y_test, feat_names = load_data(euler)
	
	# --------------------------------------------------------------------------------------------------------------------
	
	# COST SENSITIVE
	print 'part 1: cost sensitive learning ...'

	clf = RandomForestClassifier(class_weight='balanced', n_jobs=parallel_jobs)

	# create scoring object
	roc_auc_weighted = make_scorer(roc_auc_score, average='weighted')

	# fit the model and apply CV
	clf.fit(X_train,y_train)
	CV = cross_val_score(clf, X_train, y_train, cv=5, scoring=roc_auc_weighted)
	print '\nCross validation (ROC AUC): ', CV
	print 'Mean CV Score: ', np.mean(CV), '(', np.std(CV),')'
	
	# obtain probability values
	y_proba = clf.predict_proba(X_test)

	# ROC AUC score
	pred_class1 = [row[1] for row in y_proba]
	print 'ROC AUC score (test set): ', roc_auc_score(y_test, pred_class1)

	# Print ROC curve
	ROC_curve_cost_sensitive(pred_class1,y_test,euler)

	# --------------------------------------------------------------------------------------------------------------------

	# OVERSAMP RANDOM
	print 'part 2: random oversampling ...'

	clf = RandomForestClassifier(n_jobs=parallel_jobs)

	# Oversample the training set
	X_res, y_res = oversample_random(X_train, y_train)

	# fit the model and apply CV
	clf.fit(X_res,y_res)

	# obtain probability values
	y_proba = clf.predict_proba(X_test)

	# ROC AUC score
	pred_class1 = [row[1] for row in y_proba]

	# Print ROC curve
	ROC_curve_random_oversamp(pred_class1,y_test,euler)

	# --------------------------------------------------------------------------------------------------------------------
	
	# OVERSAMP SMOTE
	print 'part 3: oversampling SMOTE ...'

	clf = RandomForestClassifier(n_jobs=parallel_jobs)

	# Oversample the training set
	X_res, y_res = oversample_SMOTE(X_train, y_train)

	# fit the model and apply CV
	clf.fit(X_res,y_res)

	# Oversample the training set
	X_res, y_res = oversample_SMOTE(X_train, y_train)

	# fit the model and apply CV
	clf.fit(X_res,y_res)

	# obtain probability values
	y_proba = clf.predict_proba(X_test)

	# ROC AUC score
	pred_class1 = [row[1] for row in y_proba]

	# Print ROC curve
	ROC_curve_SMOTE(pred_class1,y_test,euler)

	# --------------------------------------------------------------------------------------------------------------------

	# UNDERSAMPLING STACKED
	print 'part 4: undersampling stacked ...'

	# create a number of balanced subsets of the training data by undersampling
	n_subsets = 10
	X_res, y_res = undersamp_easy_ensemble(n_subsets, X_train, y_train)

	# define estimator
	clf = RandomForestClassifier(n_jobs=parallel_jobs)

	# create scoring object
	roc_auc_weighted = make_scorer(roc_auc_score, average='weighted')

	clf_probs = []
	
	for i in range(0,n_subsets):
		clf.fit(X_res[i],y_res[i])
		clf_probs.append(clf.predict_proba(X_test))

	# AVERAGE VALUES
	avg_prob = np.mean( np.array( clf_probs ), axis=0 )

	# ROC AUC score
	pred_class1 = [row[1] for row in avg_prob]

	# Print ROC curve
	ROC_curve_stacked(pred_class1,y_test,euler)

	# --------------------------------------------------------------------------------------------------------------------

	# UNDERSAMPLING NEAR MISS

	print 'part 5: undersampling near miss ...'
	
	# define estimator
	clf = RandomForestClassifier(n_jobs=parallel_jobs)

	# create a balanced set of the training data by heuristic undersampling
	X_res, y_res = undersamp_near_miss(X_train, y_train, 1)

	# create scoring object
	roc_auc_weighted = make_scorer(roc_auc_score, average='weighted')
	
	# fit the model and apply CV
	clf.fit(X_res,y_res)

	# obtain probability values
	y_proba = clf.predict_proba(X_test)

	# ROC AUC score
	pred_class1 = [row[1] for row in y_proba]

	# Print ROC curve
	ROC_curve_near_miss(pred_class1, y_test, euler)

	# --------------------------------------------------------------------------------------------------------------------

	# UNDERSAMPLING RANDOM

	print 'part 6: undersampling random ...'
	
	# define estimator
	clf = RandomForestClassifier(n_jobs=parallel_jobs)
	
	# create a number of balanced subsets of the training data by undersampling
	X_res, y_res = undersamp_random(X_train, y_train)

	# create scoring object
	roc_auc_weighted = make_scorer(roc_auc_score, average='weighted')

	# fit the estimator
	clf.fit(X_res,y_res)

	# obtain probability values
	y_proba = clf.predict_proba(X_test)

	# ROC AUC score
	pred_class1 = [row[1] for row in y_proba]

	# Print ROC curve
	ROC_curve_undersamp_random(pred_class1, y_test, euler)

	# --------------------------------------------------------------------------------------------------------------------

	print '\n execution complete !'

