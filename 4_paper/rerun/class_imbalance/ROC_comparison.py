import numpy as np

import matplotlib.pyplot as plt

from sklearn.metrics import roc_auc_score, roc_curve, auc

from collections import OrderedDict

linestyles = OrderedDict(
    [('solid',               (0, ())),
     ('loosely dotted',      (0, (1, 10))),
     ('dotted',              (0, (1, 5))),
     ('densely dotted',      (0, (1, 1))),

     ('loosely dashed',      (0, (5, 10))),
     ('dashed',              (0, (5, 5))),
     ('densely dashed',      (0, (5, 1))),

     ('loosely dashdotted',  (0, (3, 10, 1, 10))),
     ('dashdotted',          (0, (3, 5, 1, 5))),
     ('densely dashdotted',  (0, (3, 1, 1, 1))),

     ('loosely dashdotdotted', (0, (3, 10, 1, 10, 1, 10))),
     ('dashdotdotted',         (0, (3, 5, 1, 5, 1, 5))),
     ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))])


# import data

# cost-sensitive
cost_sensitive_FP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_balanced_weights_FPR.csv', delimiter=',')
cost_sensitive_TP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_balanced_weights_TPR.csv', delimiter=',')
#mean_CV_cost_sensitive = 0.50
roc_auc_cost_sensitive = auc(cost_sensitive_FP, cost_sensitive_TP)

# Random oversamp
oversamp_random_FP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_ROS_FPR.csv', delimiter=',')
oversamp_random_TP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_ROS_TPR.csv', delimiter=',')
#mean_CV_oversamp_random = 0.65
roc_auc_oversamp_random = auc(oversamp_random_FP, oversamp_random_TP)

# Random undersamp I
undersamp_random_FP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_undersamp_random_FPR.csv', delimiter=',')
undersamp_random_TP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_undersamp_random_TPR.csv', delimiter=',')
#mean_CV_undersamp_random = 0.63
roc_auc_undersamp_random = auc(undersamp_random_FP, undersamp_random_TP)

# Random undersamp II
easy_ensemble_FP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_easy_ensemble_FPR.csv', delimiter=',')
easy_ensemble_TP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_easy_ensemble_TPR.csv', delimiter=',')
#mean_CV_easy_ensemble = 0.63
roc_auc_easy_ensemble = auc(easy_ensemble_FP, easy_ensemble_TP)

# SMOTE
SMOTE_FP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_SMOTE_FPR.csv', delimiter=',')
SMOTE_TP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_SMOTE_TPR.csv', delimiter=',')
#mean_CV_SMOTE = 0.62
roc_auc_SMOTE = auc(SMOTE_FP, SMOTE_TP)

# heuristic undersamp
near_miss_FP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_near_miss_FPR.csv', delimiter=',')
near_miss_TP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/class_imbalance/roc_auc_near_miss_TPR.csv', delimiter=',')
#mean_CV_near_miss = 0.98
roc_auc_near_miss = auc(near_miss_FP, near_miss_TP)

# PLOT all together
plt.title('Receiver Operating Characteristic',fontsize=22)

plt.plot(cost_sensitive_FP, cost_sensitive_TP, 'black', ls=linestyles['densely dotted'], label='Cost-Sensitive Learning: %0.2f'% (roc_auc_cost_sensitive),linewidth=2.5 )
plt.plot(oversamp_random_FP, oversamp_random_TP, 'black', ls=linestyles['dotted'], label='Random Over-Sampling: %0.2f'% (roc_auc_oversamp_random), linewidth=2.5  )
plt.plot(undersamp_random_FP, undersamp_random_TP, 'black', ls=linestyles['densely dashed'], label='Random Under-Sampling: %0.2f'% (roc_auc_undersamp_random), linewidth=2.5  )
plt.plot(easy_ensemble_FP, easy_ensemble_TP, 'black', ls=linestyles['dashed'], label='Stacked Random Under-Sampling: %0.2f'% (roc_auc_easy_ensemble),linewidth=2.5  )
plt.plot(SMOTE_FP, SMOTE_TP, 'black', ls=linestyles['densely dashdotted'], label='Heuristic Over-Sampling: %0.2f'% (roc_auc_SMOTE) ,linewidth=2.5 )
plt.plot(near_miss_FP, near_miss_TP, 'black', ls=linestyles['dashdotted'], label='Heuristic Under-Sampling: %0.2f'% (roc_auc_near_miss) ,linewidth=2.5 )

plt.legend(loc='lower right')
#plt.plot([0,1],[0,1],'--',color='black')
plt.xlim([-0.1,1.1])
plt.ylim([-0.1,1.1])
plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.3)
plt.ylabel('True Positive Rate',fontsize=18)
plt.xlabel('False Positive Rate',fontsize=18)
plt.show()