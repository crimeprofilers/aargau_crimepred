import numpy as np

import matplotlib.pyplot as plt

from sklearn.metrics import roc_auc_score, roc_curve, auc


from collections import OrderedDict

linestyles = OrderedDict(
    [('solid',               (0, ())),
     ('loosely dotted',      (0, (1, 10))),
     ('dotted',              (0, (1, 5))),
     ('densely dotted',      (0, (1, 1))),

     ('loosely dashed',      (0, (5, 10))),
     ('dashed',              (0, (5, 5))),
     ('densely dashed',      (0, (5, 1))),

     ('loosely dashdotted',  (0, (3, 10, 1, 10))),
     ('dashdotted',          (0, (3, 5, 1, 5))),
     ('densely dashdotted',  (0, (3, 1, 1, 1))),

     ('loosely dashdotdotted', (0, (3, 10, 1, 10, 1, 10))),
     ('dashdotdotted',         (0, (3, 5, 1, 5, 1, 5))),
     ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))])


# import data

# in order for the linestlyes to be visible we have to exclude parts of the data
n = 150

# URB_LEV = 0
urblev_0_FP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/geo_analysis/FPR_urblev_0.csv', delimiter=',')
urblev_0_FP = urblev_0_FP[::n]
urblev_0_TP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/geo_analysis/TPR_urblev_0.csv', delimiter=',')
urblev_0_TP = urblev_0_TP[::n]
roc_auc_urblev_0 = auc(urblev_0_FP, urblev_0_TP)

# URB_LEV = 1
urblev_1_FP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/geo_analysis/FPR_urblev_1.csv', delimiter=',')
urblev_1_FP = urblev_1_FP[::n]
urblev_1_TP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/geo_analysis/TPR_urblev_1.csv', delimiter=',')
urblev_1_TP = urblev_1_TP[::n]
roc_auc_urblev_1 = auc(urblev_1_FP, urblev_1_TP)

# URB_LEV = 2
urblev_2_FP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/geo_analysis/FPR_urblev_2.csv', delimiter=',')
urblev_2_FP = urblev_2_FP[::n]
urblev_2_TP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/geo_analysis/TPR_urblev_2.csv', delimiter=',')
urblev_2_TP = urblev_2_TP[::n]
roc_auc_urblev_2 = auc(urblev_2_FP, urblev_2_TP)

# GENERAL MODEL
gen_mod_FP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/FPR_overall.csv', delimiter=',')
gen_mod_FP = gen_mod_FP[::n]
gen_mod_TP = np.loadtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/TPR_overall.csv', delimiter=',')
gen_mod_TP = gen_mod_TP[::n]
roc_auc_gen_mod = auc(gen_mod_FP, gen_mod_TP)


# PLOT all together
plt.title('Receiver Operating Characteristic', fontsize=22)

plt.plot(gen_mod_FP, gen_mod_TP, 'black', ls=linestyles['dotted'], label=str('Overall Modell - AUC = ' + "{0:.2f}".format(roc_auc_gen_mod)), linewidth=2)#: %0.2f (CV: %0.2f)'% (roc_auc_ExtraTrees, mean_CV_ExtraTrees) )
plt.plot(urblev_0_FP, urblev_0_TP, 'black', ls=linestyles['dashed'], label= str('Urb_Lev_0 - AUC = ' + "{0:.2f}".format(roc_auc_urblev_0)), linewidth=2)# : %0.2f (CV: %0.2f)'% (roc_auc_DecisionTree, mean_CV_DecisionTree) )
plt.plot(urblev_1_FP, urblev_1_TP, 'black', ls=linestyles['dashdotted'], label=str('Urb_Lev_1 - AUC = ' + "{0:.2f}".format(roc_auc_urblev_1)), linewidth=2)#: %0.2f (CV: %0.2f)'% (roc_auc_RandomForest, mean_CV_RandomForest) )
plt.plot(urblev_2_FP, urblev_2_TP, 'black', ls=linestyles['dashdotdotted'], label=str('Urb_Lev_2 - AUC = ' + "{0:.2f}".format(roc_auc_urblev_2)), linewidth=2)#: %0.2f (CV: %0.2f)'% (roc_auc_ExtraTrees, mean_CV_ExtraTrees) )


plt.legend(loc='lower right')
#plt.plot([0,1],[0,1],'--',color='black')
plt.xlim([-0.1,1.1])
plt.ylim([-0.1,1.1])
plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.3)
plt.ylabel('True Positive Rate', fontsize=18)
plt.xlabel('False Positive Rate', fontsize=18)
plt.show()