import numpy as np

import pandas as pd

from sklearn.metrics import roc_auc_score, roc_curve, auc

import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt

from tqdm import tqdm

def ROC_curve(pred_class1,y_test):
	
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
	roc_auc = auc(false_positive_rate, true_positive_rate)

	plt.title('Receiver Operating Characteristic')
	plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC = %0.2f'% roc_auc)
	plt.legend(loc='lower right')
	plt.plot([0,1],[0,1],'r--')
	plt.xlim([-0.1,1.1])
	plt.ylim([-0.1,1.1])
	plt.ylabel('True Positive Rate')
	plt.xlabel('False Positive Rate')
	
	plt.savefig(str('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/ROC_curve_overall.png'))
	np.savetxt(str('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/FPR_overall.csv'), false_positive_rate, delimiter=',')
	np.savetxt(str('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/TPR_overall.csv'), true_positive_rate, delimiter=',')


def PAI(y_pred,y_test,coverage):
		
	# Predictive Accuracy Index (PAI) for every day
	# one day has 10149 cells
	# the testing set contains 366 days
	#print '...calculating the PAI...'

	PAI_list = []
	N_list = []
	n_list = []
	alert_list = []
	nN_list = []
	aA_list = []

	for day in range(366):
		alert_level_daily = alert_level[(day*10149):((day+1)*10149)]
		test = y_test[(day*10149):((day+1)*10149)]

		num_samples = round(len(alert_level_daily)*coverage)
		index_list = np.argsort(alert_level_daily)[-int(num_samples):]

		pred = np.zeros((alert_level_daily.shape[0]))
		pred[index_list] = 1
		
		n = float(np.sum((test == pred) & (test == 1))) # number of crimes within predictive hotspots
		N = float(np.count_nonzero(test)) # total number of crimes
		a = float(4*np.count_nonzero(pred)) # total area of hotspots
		A = float(4*10149) # total area 
		
		#print n, N, np.count_nonzero(pred)
		N_list.append(N)
		n_list.append(n)
		alert_list.append(np.count_nonzero(pred))
		aA_list.append(a/A)

		if N == 0 or a == 0:
			PAI_list.append(0)
			nN_list.append(0)
		else:
			PAI_list.append((n/N)/(a/A))
			nN_list.append(n/N)
	
	# SAVE PAI_list			
	#np.savetxt("/Users/rudimac/Documents/MTEC/THESIS/MODELS/PAI_list_undersamp.csv", PAI_list, delimiter=",")
	#np.savetxt(str("/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/geographic_error/PAI_lists/PAI_list_ml_cov" + str((coverage*10149*4)/140400*100) + str(".csv")), PAI_list, delimiter=",")

	return (coverage*10149*4)/140400*100, np.mean(nN_list), np.std(nN_list) #, np.mean(nN_list)
	#return (coverage*10149*4)/140400*100, np.mean(PAI_list), np.std(PAI_list) #, np.mean(nN_list)
	#return PAI_list

# import avg_impo
avg_prob = np.genfromtxt('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/avg_prob_total_model.csv', delimiter=',')

# rearrange avg_prob
alert_level = []
for p in avg_prob:
	alert_level.append(p[1])
alert_level = np.array(alert_level)

# import y_test
target = pd.read_csv('/Users/rudimac/Documents/MTEC/THESIS/FEATURES/dataset_final.csv', index_col=[1,2])['to_date']
target = target.astype(int)
y_test = target.loc['2016-01-14':'2017-01-13',:].values

# ROC curve
pred_class1 = [row[1] for row in avg_prob]
ROC_curve(pred_class1, y_test)

# Define prediction coverage area
#pred_cov_list = [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1]
pred_cov_list = np.arange(0.01,1.01,0.01)

cov_vs_hit = []

for pred_cov in tqdm(pred_cov_list):

	pred_cov_grid = (140400*pred_cov)/(10149*4)
	#print '\n\nCOVERAGE: ', pred_cov,'/', pred_cov_grid,'\n'

	# Print PAI
	cov_vs_hit.append( PAI(alert_level,y_test,pred_cov_grid) )

	# Probability vote with threshold
	#y_pred = prob_vote(found_threshold)
	#np.savetxt(str("/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/PAI/PAI_list_ml_cov" + str(pred_cov) + str(".csv")), PAI(alert_level,y_test,pred_cov_grid), delimiter=",")

#np.savetxt("/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/PAI_list_ml.csv", cov_vs_hit, delimiter=",")
#np.savetxt("/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/cov_vs_hit_ml.csv", cov_vs_hit, delimiter=",")
