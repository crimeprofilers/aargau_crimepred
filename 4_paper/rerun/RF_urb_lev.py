import numpy as np

import pandas as pd

import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt

from random import randint

from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import cross_val_score
from sklearn.metrics import roc_auc_score, roc_curve, auc, confusion_matrix, fbeta_score, make_scorer
from sklearn.grid_search import GridSearchCV

from imblearn.ensemble import EasyEnsemble


def load_data(euler):

	print '...importing data...'
	if euler == 0:
		data = pd.read_csv('/Users/rudimac/Documents/MTEC/THESIS/FEATURES/dataset_final.csv', index_col=[1,2]).drop(['Unnamed: 0'],axis=1)
	elif euler == 1:
		data = pd.read_csv('/cluster/home/rudolfm/csv/all_data.csv', index_col=[0,1])
	else:
		print 'Specify data path!'

	print '\n...adding urb_lev column...'
	data['urb_lev'] = np.zeros(data.shape[0]).astype(int)

	# assign level of urbanization according to natural breaks thresholds
	#   jenks algorithm: [0.0, 18.25, 54.25, 250.75]
	#   percentiles: [0.0 2.25 16.75 250.75]
	print '   PERCENTILES'
	print '   33%: ', np.percentile(data.popdens, 33.33)
	print '   66%: ', np.percentile(data.popdens, 66.66)
	data.loc[(data['popdens'] >= np.percentile(data.popdens, 33.33)) & (data['popdens'] < np.percentile(data.popdens, 66.66)), 'urb_lev'] = 1
	data.loc[(data['popdens'] >= np.percentile(data.popdens, 66.66)), 'urb_lev'] = 2

	return data

def traintest_split(data,urb_level):

	print '\n...splitting into features and target...'

	# only one type of urbanization level!
	features = data[data['urb_lev']==urb_level].drop(['offence','probability','offence_ids','pred_class','probability_norm','pred_class_norm','pred_class_bin','urb_lev','to_date'],axis=1)
	target = data[data['urb_lev']==urb_level].to_date
	target = target.astype(int)

	print '\n\nFEATURE SHAPE: ',features.shape,'TARGET SHAPE: ', target.shape

	feat_names = features.columns

	print '\n   Total Number of grid cells: ', features.loc['2014-01-14',:].shape[0]
	#print '\nTotal Number of days: ', features.loc[(slice('2014-01-14','2017-01-13'),1429),:].shape[0]
	#print 'Number of training days: ', features.loc[(slice('2014-01-14','2016-01-13'),1429),:].shape[0]
	#print 'Number of testing days: ', features.loc[(slice('2016-01-14','2017-01-13'),1429),:].shape[0]

	print '\n   Class distributions - Original Dataset'
	print '      Class 0: ', target.value_counts()[0], '(',float(target.value_counts()[0])/target.shape[0]*100, '% )'
	print '      Class 1: ', target.value_counts()[1], '(',float(target.value_counts()[1])/target.shape[0]*100, '% )'

	# split dataset into training and testing data
	X_train = features.loc['2014-01-14':'2016-01-13',:].values
	y_train = target.loc['2014-01-14':'2016-01-13',:].values
	X_test = features.loc['2016-01-14':'2017-01-13',:].values
	y_test = target.loc['2016-01-14':'2017-01-13',:].values

	tot = len(y_train)
	print '\n   Class distributions - Training Dataset'
	print '      Class 0: ', np.count_nonzero(y_train == 0), '(',float(np.count_nonzero(y_train == 0))/tot*100, '% )'
	print '      Class 1: ', np.count_nonzero(y_train == 1), '(',float(np.count_nonzero(y_train == 1))/tot*100, '% )'

	tot = len(y_test)
	print '\n   Class distributions - Testing Dataset'
	print '      Class 0: ', np.count_nonzero(y_test == 0), '(',float(np.count_nonzero(y_test == 0))/tot*100, '% )'
	print '      Class 1: ', np.count_nonzero(y_test == 1), '(',float(np.count_nonzero(y_test == 1))/tot*100, '% )'

	return X_train,y_train,X_test,y_test,feat_names

def undersamp_easy_ensemble(n_subsets, X_train, y_train):

	print '\n...resampling...'
	# iteratively select a random subset and make an ensemble of the different sets
	print '\n   Number of undersampling subsets:', n_subsets, '\n'
	ee = EasyEnsemble(n_subsets=n_subsets, random_state=4242)
	X_res, y_res = ee.fit_sample(X_train, y_train)

	print '\n   Balanced Dataset: '
	print '      Class 0: ', np.count_nonzero(y_res[0] == 0)
	print '      Class 1: ', np.count_nonzero(y_res[0] == 1)

	return X_res,y_res

def ROC_curve(pred_class1,y_test,euler,urb_lev):
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
	roc_auc = auc(false_positive_rate, true_positive_rate)

	plt.title('Receiver Operating Characteristic')
	plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC = %0.2f'% roc_auc)
	plt.legend(loc='lower right')
	plt.plot([0,1],[0,1],'r--')
	plt.xlim([-0.1,1.1])
	plt.ylim([-0.1,1.1])
	plt.ylabel('True Positive Rate')
	plt.xlabel('False Positive Rate')

	if euler == 0:
		plt.savefig(str('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/geographic_error/ML_model_urblev/ROC_curve_urblev_')+ str(urb_lev) + str('.png'))
		np.savetxt(str('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/geographic_error/ML_model_urblev/FPR_urblev_'+ str(urb_lev) + str('.csv')), false_positive_rate, delimiter=',')
		np.savetxt(str('/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/geographic_error/ML_model_urblev/TPR_urblev_'+ str(urb_lev) + str('.csv')), true_positive_rate, delimiter=',')
	elif euler == 1:
		plt.savefig('/cluster/home/rudolfm/results2/ROC_curve_random_forest.png')
		np.savetxt('/cluster/home/rudolfm/results2/roc_auc_random_forest_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/cluster/home/rudolfm/results2/roc_auc_random_forest_TPR.csv', true_positive_rate, delimiter=',')

def FEAT_imp(avg_impo, avg_impo_std, feat_names):

	indices = np.argsort(avg_impo)[::-1]
	feat_list = []
	
	# Print the feature ranking
	print('\nFeature ranking:')
	print '--------------'

	for f in range(len(feat_names)):
		print("%d. Feature: %s (%f)" % (f + 1, feat_names[indices[f]], avg_impo[indices[f]])), 'STD: ', avg_impo_std[indices[f]]
		feat_list.append( [ f + 1, feat_names[indices[f]], avg_impo[indices[f]], avg_impo_std[indices[f]] ] )

	# save to csv
	np.savetxt(str("/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/geographic_error/ML_model_urblev/feat_imp_urblev_")+str(urb_level), feat_list, delimiter=",")


if __name__ == '__main__':

	print '\n\n\n\n\n\n'
	
	# local = 0 or cluster = 1:
	euler = 0


	# load data
	data = load_data(euler)

	# three different models according to the level of urbanization
	for urb_level in range(3):

		# split into train and test set
		X_train, y_train, X_test, y_test, feat_names = traintest_split(data,urb_level)

		# create a number of balanced subsets of the training data by undersampling
		n_subsets = 10
		X_res, y_res = undersamp_easy_ensemble(n_subsets, X_train, y_train)

		# define estimator
		clf = RandomForestClassifier(n_jobs=-1)

		# create scoring object
		roc_auc_weighted = make_scorer(roc_auc_score, average='weighted')

		# define grid containing a set of hyperparameters
		param_grid = { 
	    'n_estimators': [700, 1000, 1500],
	    'max_features': ['auto', 'sqrt', 'log2']
		}

		CV_clf = GridSearchCV(estimator=clf, param_grid=param_grid, cv= 5, scoring='roc_auc')

		
		print '\n...training the classifier...'
		clf_probs = []
		cross_val = []
		importances = []
		importances_std = []

		for i in range(0,n_subsets):
			
			#print '    ...hyperparameter tuning (subset: ', i, ')...'
			CV_clf.fit(X_res[i],y_res[i])

			#print '        \nThe best score is: ', CV_clf.best_score_
			#print '        ( ', CV_clf.best_params_, ')'
			hyperpara = CV_clf.best_params_
			cross_val.append(CV_clf.best_score_)

			# assign optimal hyperparamters to estimator and fit the model
			clf = RandomForestClassifier(n_estimators=hyperpara['n_estimators'], max_features=hyperpara['max_features'], n_jobs=-1)

			#print '        ...making predictions...\n'
			clf.fit(X_res[i],y_res[i])
			clf_probs.append(clf.predict_proba(X_test))
			
			# obtain feature importance values
			importances.append(clf.feature_importances_)
			importances_std.append(np.std([tree.feature_importances_ for tree in clf.estimators_],axis=0))

		# AVERAGE VALUES
		avg_prob = np.mean( np.array( clf_probs ), axis=0 )
		avg_cv = np.mean( np.array( cross_val ), axis=0 )
		avg_impo = np.mean( np.array( importances ), axis=0 )
		avg_impo_std = np.mean( np.array( importances_std ), axis=0 )
	
		# save data
		np.savetxt(str("/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/avg_prob_0")+str(urb_level)+str('.csv'), avg_prob, delimiter=",")

		# Print average feature importance
		FEAT_imp(avg_impo, avg_impo_std, feat_names)

		# ROC AUC score
		pred_class1 = [row[1] for row in avg_prob]
		ROC_curve(pred_class1, y_test, euler, urb_level)

		

	print '\n-------------------------------------------------------------------------------------------------------------------\n'
	print 'Execution complete'
	print '\n-------------------------------------------------------------------------------------------------------------------\n'

