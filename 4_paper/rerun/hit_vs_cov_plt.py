import numpy as np

import pandas as pd

import matplotlib.pyplot as plt

# load data

ML_overall_canton = pd.read_csv("/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/cov_vs_hit_ml.csv",header=None)
ML_overall_canton.columns = ['coverage','hitrate','STD']
hotspot_canton = pd.read_csv("/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/hotspot/cov_vs_hit_HOTSPOT.csv",header=None)
hotspot_canton.columns = ['coverage','hitrate','STD']

#plt.title('Hit Rate vs. Coverage Area', fontsize=20)

plt.plot(ML_overall_canton.coverage, ML_overall_canton.hitrate, label='Machine Learning', linewidth=2, color = 'black', ls='--')
#plt.fill_between(ML_overall_canton.coverage, ML_overall_canton.hitrate - ML_overall_canton.STD, ML_overall_canton.hitrate + ML_overall_canton.STD, color = 'blue', alpha = 0.4)
plt.plot(hotspot_canton.coverage, hotspot_canton.hitrate, label='Hotspot', linewidth=2, color = 'black', ls=':')
#plt.fill_between(hotspot_canton.coverage, hotspot_canton.hitrate - hotspot_canton.STD, hotspot_canton.hitrate + hotspot_canton.STD, color = 'green', alpha = 0.4)


plt.legend(loc='lower right')
plt.xlim([-0,50])
plt.ylim([-0,1])
plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.3)
plt.ylabel('Hit Rate', fontsize=18)
plt.xlabel('Coverage Area', fontsize=18)
plt.show()