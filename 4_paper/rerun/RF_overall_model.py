import numpy as np

import pandas as pd

import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt

from random import randint

from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import cross_val_score
from sklearn.metrics import roc_auc_score, roc_curve, auc, confusion_matrix, fbeta_score, make_scorer
from sklearn.grid_search import GridSearchCV

from imblearn.ensemble import EasyEnsemble


def load_data(euler):

	print '...importing data...'
	if euler == 0:
		data = pd.read_csv('/Users/rudimac/Documents/MTEC/THESIS/FEATURES/dataset_final.csv', index_col=[1,2]).drop(['Unnamed: 0'],axis=1)
	elif euler == 1:
		data = pd.read_csv('/cluster/home/rudolfm/csv/all_data.csv', index_col=[0,1])
	else:
		print 'Specify data path!'

	print 'DATA SHAPE: ', data.shape
	print '...splitting into features and target...'
	features = data.drop(['offence','probability','offence_ids','pred_class','probability_norm','pred_class_norm','pred_class_bin','to_date'],axis=1)
	target = data['to_date']
	target = target.astype(int)

	#print features.columns
	print '\n\nFEATURE SHAPE: ',features.shape,'TARGET SHAPE: ', target.shape

	feat_names = features.columns

	print '\nTotal Number of cells: ', features.loc['2014-01-14',:].shape[0]
	print '\nTotal Number of days: ', features.loc[(slice('2014-01-14','2017-01-13'),1429),:].shape[0]
	print 'Number of training days: ', features.loc[(slice('2014-01-14','2016-01-13'),1429),:].shape[0]
	print 'Number of testing days: ', features.loc[(slice('2016-01-14','2017-01-13'),1429),:].shape[0]

	print '\nClass distributions - Original Dataset'
	print 'Class 0: ', target.value_counts()[0], '(',float(target.value_counts()[0])/target.shape[0]*100, '% )'
	print 'Class 1: ', target.value_counts()[1], '(',float(target.value_counts()[1])/target.shape[0]*100, '% )'

	# split dataset into training and testing data
	X_train = features.loc['2014-01-14':'2016-01-13',:].values
	y_train = target.loc['2014-01-14':'2016-01-13',:].values
	X_test = features.loc['2016-01-14':'2017-01-13',:].values
	y_test = target.loc['2016-01-14':'2017-01-13',:].values

	tot = len(y_train)
	print '\nClass distributions - Training Dataset'
	print 'Class 0: ', np.count_nonzero(y_train == 0), '(',float(np.count_nonzero(y_train == 0))/tot*100, '% )'
	print 'Class 1: ', np.count_nonzero(y_train == 1), '(',float(np.count_nonzero(y_train == 1))/tot*100, '% )'

	tot = len(y_test)
	print '\nClass distributions - Testing Dataset'
	print 'Class 0: ', np.count_nonzero(y_test == 0), '(',float(np.count_nonzero(y_test == 0))/tot*100, '% )'
	print 'Class 1: ', np.count_nonzero(y_test == 1), '(',float(np.count_nonzero(y_test == 1))/tot*100, '% )'

	return X_train,y_train,X_test,y_test,feat_names

def undersamp_easy_ensemble(n_subsets, X_train, y_train):

	print '\n...resampling...'
	# iteratively select a random subset and make an ensemble of the different sets
	print '\nNumber of undersampling subsets: ____', n_subsets, '____\n'
	ee = EasyEnsemble(n_subsets=n_subsets, random_state=4242)
	X_res, y_res = ee.fit_sample(X_train, y_train)

	print '\nBalanced Dataset: '
	print 'Class 0: ', np.count_nonzero(y_res[0] == 0)
	print 'Class 1: ', np.count_nonzero(y_res[0] == 1)

	return X_res,y_res

def FEAT_imp(avg_impo, avg_impo_std, feat_names):

	indices = np.argsort(avg_impo)[::-1]
	impo_list = []
	
	# Print the feature ranking
	print('\nFeature ranking:')
	print '--------------'

	for f in range(len(feat_names)):
		print("%d. Feature: %s (%f)" % (f + 1, feat_names[indices[f]], avg_impo[indices[f]])), 'STD: ', avg_impo_std[indices[f]]
		impo_list.append( [feat_names[indices[f]], avg_impo[indices[f]], avg_impo_std[indices[f]] ] ) 

	return impo_list

	# Plot the feature importances of the forest
	#plt.figure(figsize=(30,8))
	#plt.title("Feature importances")
	#plt.bar(range(len(feat_names)), avg_impo[indices],color="r", yerr=avg_impo_std[indices], align="center")
	#plt.xticks(range(len(feat_names)), feat_names[indices])
	#plt.xlim([-1,20])


if __name__ == '__main__':

	print '\n\n\n\n\n\n'
	
	# local = 0 or cluster = 1:
	euler = 0

	# load data and split into train and test set
	X_train, y_train, X_test, y_test, feat_names = load_data(euler)

	# create a number of balanced subsets of the training data by undersampling
	n_subsets = 10
	X_res, y_res = undersamp_easy_ensemble(n_subsets, X_train, y_train)

	# define estimator
	clf = RandomForestClassifier(n_jobs=-1)

	# create scoring object
	roc_auc_weighted = make_scorer(roc_auc_score, average='weighted')

	# define grid containing a set of hyperparameters
	param_grid = { 
    'n_estimators': [700, 1000, 1500],
    'max_features': ['auto', 'sqrt', 'log2']
	}

	CV_clf = GridSearchCV(estimator=clf, param_grid=param_grid, cv= 5, scoring='roc_auc')

	
	print '\n...training the classifier...'
	clf_probs = []
	cross_val = []
	importances = []
	importances_std = []

	for i in range(0,n_subsets):
		
		print '    ...hyperparameter tuning (subset: ', i, ')...'
		CV_clf.fit(X_res[i],y_res[i])

		print '        \nThe best score is: ', CV_clf.best_score_
		print '        ( ', CV_clf.best_params_, ')'
		hyperpara = CV_clf.best_params_
		cross_val.append(CV_clf.best_score_)

		# assign optimal hyperparamters to estimator and fit the model
		clf = RandomForestClassifier(n_estimators=hyperpara['n_estimators'], max_features=hyperpara['max_features'], n_jobs=-1)

		print '        ...making predictions...\n'
		clf.fit(X_res[i],y_res[i])
		clf_probs.append(clf.predict_proba(X_test))
		
		# obtain feature importance values
		importances.append(clf.feature_importances_)
		importances_std.append(np.std([tree.feature_importances_ for tree in clf.estimators_],axis=0))

	# AVERAGE VALUES
	avg_prob = np.mean( np.array( clf_probs ), axis=0 )
	avg_cv = np.mean( np.array( cross_val ), axis=0 )
	avg_impo = np.mean( np.array( importances ), axis=0 )
	avg_impo_std = np.mean( np.array( importances_std ), axis=0 )

	# save average probabilities
	np.savetxt("/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/avg_prob_total_model.csv", avg_prob, delimiter=",")


	# Print average feature importance
	np.savetxt("/Users/rudimac/Documents/MTEC/THESIS/git/aargau_crimepred/4_paper/rerun/feature_importances/feat_impo_overall_model.csv", FEAT_imp(avg_impo, avg_impo_std, feat_names), delimiter=",")
	


	print '\n-------------------------------------------------------------------------------------------------------------------\n'
	print 'Execution complete'
	print '\n-------------------------------------------------------------------------------------------------------------------\n'

