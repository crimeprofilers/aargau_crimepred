#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 01:13:15 2018

@author: ckadar

"""

import sys
import pandas as pd
import numpy as np

from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.grid_search import GridSearchCV

from imblearn.ensemble import EasyEnsemble

data_path = 'dataset_final.csv'
stacking_ensemble_proba_path = "avg_test_proba_stacking_ensemble_all_features.csv"
stacking_ensemble_features_path = "feat_impo_stacking_ensemble_all_features.npy"
out_path = "stacking_ensemble_out.txt"

def undersamp_easy_ensemble(n_subsets, X_train, y_train):
    print('Resampling...')
    # iteratively select a random subset, replacement = False by default
    ee = EasyEnsemble(n_subsets=n_subsets, random_state=42)
    X_res, y_res = ee.fit_sample(X_train, y_train)
    for i in range(n_subsets):
        print('Balanced Dataset: ', i)
        print('Class 0: ', np.count_nonzero(y_res[i] == 0))
        print('Class 1: ', np.count_nonzero(y_res[i] == 1))
    return X_res,y_res


def feature_imp(avg_impo, avg_impo_std, feat_names):
    print('Feature ranking...')
    indices = np.argsort(avg_impo)[::-1]
    impo_list = []
    
    for f in range(len(feat_names)):
        print("%d. Feature: %s (%f)" % (f + 1, feat_names[indices[f]], avg_impo[indices[f]])), 'STD: ', avg_impo_std[indices[f]]
        impo_list.append([feat_names[indices[f]], avg_impo[indices[f]], avg_impo_std[indices[f]]]) 
        
    # plot the feature importances of the forest
    
    #plt.figure(figsize=(30,8))
    #plt.title("Feature importances")
    #plt.bar(range(len(feat_names)), avg_impo[indices],color="r", yerr=avg_impo_std[indices], align="center")
    #plt.xticks(range(len(feat_names)), feat_names[indices])
    #plt.xlim([-1,20])
    
    return impo_list

def load_data():
    print('---------------------------------')
    print("Reading in data...")
    data = pd.read_csv(data_path,index_col=[1,2]).drop(['Unnamed: 0'],axis=1)  
    print(data.info)
    ##%%

    print(data.shape)
    ##(11123304, 72)
    
    all_columns = data.columns
    print(all_columns)
    
    features = data.drop(['offence','probability','offence_ids','pred_class','probability_norm','pred_class_norm','pred_class_bin','to_date'],axis=1)
    
    target = data['to_date']
    target = target.astype(int)

    print('Features shape: ',features.shape,'Target shape: ', target.shape)
    ##Features shape:  (11123304, 64) Target shape:  (11123304,)
    ##%%
    
    feat_names = features.columns
    
    print('Total Number of cells: ', features.loc['2014-01-14',:].shape[0])
    print('Total Number of days: ', features.loc[(slice('2014-01-14','2017-01-13'),1429),:].shape[0])
    print('Number of training days: ', features.loc[(slice('2014-01-14','2016-01-13'),1429),:].shape[0])
    print('Number of testing days: ', features.loc[(slice('2016-01-14','2017-01-13'),1429),:].shape[0])
    ##Total Number of cells:  10149
    ##Total Number of days:  1096
    ##Number of training days:  730
    ##Number of testing days:  366
    
    print(feat_names)
    print(len(feat_names))
    
    ##%%
    print ('Class distributions - Original dataset of built land-use')
    print('Class 0: ', target.value_counts()[0], '(',float(target.value_counts()[0])/target.shape[0]*100, '% )')
    print('Class 1: ', target.value_counts()[1], '(',float(target.value_counts()[1])/target.shape[0]*100, '% )')
    ##Class 0:  11117038 ( 99.94366781668468 % )
    ##Class 1:  6266 ( 0.0563321833153171 % )
    
    ## split dataset into training and testing data
    X_train = features.loc['2014-01-14':'2016-01-13',:].values
    y_train = target.loc['2014-01-14':'2016-01-13',:].values
    X_test = features.loc['2016-01-14':'2017-01-13',:].values
    y_test = target.loc['2016-01-14':'2017-01-13',:].values

    tot = len(y_train)
    print('Class distributions - Training')
    print('Class 0: ', np.count_nonzero(y_train == 0), '(',float(np.count_nonzero(y_train == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_train == 1), '(',float(np.count_nonzero(y_train == 1))/tot*100, '% )')
    ##Class 0:  7404388 ( 99.94085387992878 % )
    ##Class 1:  4382 ( 0.05914612007121291 % )
    
    tot = len(y_test)
    print('Class distributions - Testing ')
    print('Class 0: ', np.count_nonzero(y_test == 0), '(',float(np.count_nonzero(y_test == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_test == 1), '(',float(np.count_nonzero(y_test == 1))/tot*100, '% )')
    ##Class 0:  3712650 ( 99.94928031349289 % )
    ##Class 1:  1884 ( 0.05071968650710
    
    return X_train,y_train,X_test,y_test,feat_names

def main():
    sys.stdout = open(out_path, 'w')
    
    print("DOING DATA READING")
    sys.stdout.flush()
    #read in data
    X_train, y_train, X_test, y_test, all_features = load_data()
    print(sum(y_test))
    
    print("DOING DATA SCALING")
    sys.stdout.flush()
    scaling = StandardScaler()
    #for non-DT based models we need to do feature scalling
    X_train = scaling.fit_transform(X_train) ##here use fit and transform
    X_test = scaling.transform(X_test)
  
    print("DOING DATA SUBSAMPLING")
    sys.stdout.flush()
    # create a number of balanced subsets of the training data by undersampling
    n_subsets = 10 #1
    X_res, y_res = undersamp_easy_ensemble(n_subsets, X_train, y_train)
   
    # define grid containing a set of hyperparameters
    feature_count = X_res.shape[2]
    print(feature_count)

    param_grid_LR = { 
                  'penalty': ['l2'],
                  'C': [1000, 100, 10, 1, 0.1, 0.01, 0.001]
                }

    param_grid_LRl1 = { 
                  'penalty': ['l1'],
                  'C': [1000, 100, 10, 1, 0.1, 0.01, 0.001]
                }
                
    param_grid_RF = { #'n_estimators': [10]
                  'n_estimators': [500, 1000, 1500],
                  'max_depth': [int(feature_count/3), int(feature_count/2), None]
                  #'max_features': ['auto', 'sqrt', 'log2']
                }
    param_grid_AB = { 
                  'n_estimators': [500, 1000, 1500],
                  'learning_rate': [0.01, 0.1, 1]
                }
    
    print("DOING ML TRAINING")
    sys.stdout.flush()
    print('Training the stacking ensemble...')
    
    #test_scores = []
    test_probas = []

    #cv_scores = []


    #importances = []
    #importances_std = []

    for i in range(0,n_subsets):
        print('...hyperparameter tuning (subset: ', i, ')')
        sys.stdout.flush()
        probabilities_sample = []
        
        # define the estimator
        RF_clf = RandomForestClassifier(n_jobs=-1)
        # define the CV estimator
        RF_CV_clf = GridSearchCV(estimator=RF_clf, param_grid=param_grid_RF, cv= 5, scoring='roc_auc')
        # fit the CV estimator
        RF_CV_clf.fit(X_res[i],y_res[i])
        #extract best score, best parameters, best model of the CV on this subset
        RF_best_score = RF_CV_clf.best_score_
        print('        The best score is: ', RF_best_score)
        RF_best_params = RF_CV_clf.best_params_
        print('        The best parameters are: ',RF_best_params)
        RF_best_model = RF_CV_clf.best_estimator_
        print('        The best model is: ', RF_best_model)
        sys.stdout.flush()
        RF_y_preds_proba = RF_best_model.predict_proba(X_test)
        probabilities_sample.append(RF_y_preds_proba)
        
        # define the estimator
        AB_clf = AdaBoostClassifier()
        # define the CV estimator
        AB_CV_clf = GridSearchCV(estimator=AB_clf, param_grid=param_grid_AB, cv= 5, scoring='roc_auc')
        # fit the CV estimator
        AB_CV_clf.fit(X_res[i],y_res[i])
        #extract best score, best parameters, best model of the CV on this subset
        AB_best_score = AB_CV_clf.best_score_
        print('        The best score is: ', AB_best_score)
        AB_best_params = AB_CV_clf.best_params_
        print('        The best parameters are: ',AB_best_params)
        AB_best_model = AB_CV_clf.best_estimator_
        print('        The best model is: ', AB_best_model)
        sys.stdout.flush()
        AB_y_preds_proba = AB_best_model.predict_proba(X_test)
        probabilities_sample.append(AB_y_preds_proba)
        
        # define the estimator
        LR_clf = LogisticRegression(n_jobs=-1)
        # define the CV estimator
        LR_CV_clf = GridSearchCV(estimator=LR_clf, param_grid=param_grid_LR, cv= 5, scoring='roc_auc')
        # fit the CV estimator
        LR_CV_clf.fit(X_res[0],y_res[0])
        #extract best score, best parameters, best model of the CV on this subset
        LR_best_score = LR_CV_clf.best_score_
        print('        The best score is: ', LR_best_score) 
        LR_best_params = LR_CV_clf.best_params_
        print('        The best parameters are: ', LR_best_params)
        LR_best_model = LR_CV_clf.best_estimator_
        print('        The best model is: ', LR_best_model)
        sys.stdout.flush()
        LR_y_preds_proba = LR_best_model.predict_proba(X_test)
        probabilities_sample.append(LR_y_preds_proba)
        
        # define the estimator
        LRl1_clf = LogisticRegression(n_jobs=-1)
        # define the CV estimator
        LRl1_CV_clf = GridSearchCV(estimator=LRl1_clf, param_grid=param_grid_LRl1, cv= 5, scoring='roc_auc')
        # fit the CV estimator
        LRl1_CV_clf.fit(X_res[0],y_res[0])
        #extract best score, best parameters, best model of the CV on this subset
        LRl1_best_score = LRl1_CV_clf.best_score_
        print('        The best score is: ', LRl1_best_score) 
        LRl1_best_params = LRl1_CV_clf.best_params_
        print('        The best parameters are: ', LRl1_best_params)
        LRl1_best_model = LRl1_CV_clf.best_estimator_
        print('        The best model is: ', LRl1_best_model)
        sys.stdout.flush()
        LRl1_y_preds_proba = LRl1_best_model.predict_proba(X_test)
        probabilities_sample.append(LRl1_y_preds_proba)
        
        print("        DOING STACKING AVERAGING")
        sys.stdout.flush()
        #compute average of the different models on the subsample
        y_preds_proba = np.mean( np.array(probabilities_sample), axis=0)
        test_probas.append(y_preds_proba)
        
    print("DOING ENSEMBLE AVERAGING")
    sys.stdout.flush()
    # compute averages of the ensemble
    avg_test_proba = np.mean( np.array(test_probas), axis=0)

    #avg_cv = np.mean( np.array( cv_scores ), axis=0 )
    #avg_impo = np.mean( np.array( importances ), axis=0 )
    #avg_impo_std = np.mean( np.array( importances_std ), axis=0 )
      
    print("DOING DATA SAVING")
    sys.stdout.flush()
    # save average probabilities 
    np.savetxt(stacking_ensemble_proba_path, avg_test_proba, delimiter=",")

    # save average feature importance + STD + name
    #print(all_features)
    #features_list = feature_imp(avg_impo, avg_impo_std, all_features)
    #np.save(RF_ensemble_features_path,features_list)


main()

	