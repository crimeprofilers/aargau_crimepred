#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 12:04:49 2018

@author: ckadar
"""
import pandas as pd
import numpy as np
import sklearn.linear_model as sklm
import sklearn.ensemble as skens
import sklearn.model_selection as skms
from sklearn.metrics import roc_curve, auc, confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import cross_val_score
from sklearn.metrics import roc_auc_score, roc_curve, auc, confusion_matrix, fbeta_score, make_scorer
from sklearn.grid_search import GridSearchCV

import matplotlib.pyplot as plt
from collections import Counter
import itertools
from tqdm import tqdm

from imblearn.under_sampling import RandomUnderSampler, NearMiss, ClusterCentroids
from imblearn.over_sampling import SMOTE 
from imblearn.datasets import make_imbalance
from imblearn.ensemble import BalancedBaggingClassifier
from imblearn.metrics import classification_report_imbalanced
from sklearn.ensemble import BaggingClassifier
from imblearn.ensemble import EasyEnsemble

data_path = 'dataset_final.csv'

dummy_coverage_path = "dummy_coverage.csv"

ensemble_probas_path = "proba_ensemble_ensemble_all_features.npy"
stacking_ensemble_proba_path = "avg_test_proba_stacking_ensemble_all_features.csv"
stacking_ensemble_coverage_path = "coverage_stacking_ensemble_all_features.csv"

RF_ensemble_proba_path = "avg_test_proba_RF_ensemble_all_features.csv"
RF_ensemble_features_path = "feat_impo_RF_ensemble_all_features.npy"
RF_ensemble_coverage_path = "coverage_RF_ensemble_all_features.csv"

LR_stacking_proba_path = "proba_LR_stacking_undersampling_all_features.csv"

RF_undersampling_proba_path = "proba_RF_undersampling_all_features.csv"
RF_undersampling_features_path = "feat_impo_RF_undersampling_all_features.npy"
RF_undersampling_coverage_path = "coverage_RF_undersampling_all_features.csv"

AB_undersampling_proba_path = "proba_AB_undersampling_all_features.csv"
AB_undersampling_features_path = "feat_impo_AB_undersampling_all_features.npy"
AB_undersampling_coverage_path = "coverage_AB_undersampling_all_features.csv"

LR_undersampling_proba_path = "proba_LR_undersampling_all_features.csv"
LR_undersampling_coverage_path = "coverage_LR_undersampling_all_features.csv"

LRl1_undersampling_proba_path = "proba_LRl1_undersampling_all_features.csv"
LRl1_undersampling_coverage_path = "coverage_LRl1_undersampling_all_features.csv"

RF_heuristic_undersampling_proba_path = "proba_RF_heuristic_undersampling_all_features.csv"
RF_heuristic_undersampling_features_path = "feat_impo_RF_heuristic_undersampling_all_features.npy"
RF_heuristic_undersampling_coverage_path = "coverage_RF_heuristic_undersampling_all_features.csv"

RF_ensemble_proba_path_only_crime = "avg_test_proba_RF_ensemble_onlycrime_features.csv"
RF_ensemble_features_path_only_crime = "feat_impo_RF_ensemble_onlycrime_features.npy"
RF_ensemble_coverage_path_only_crime = "coverage_RF_ensemble_only_crime.csv"

RF_ensemble_proba_path_only_spatial = "avg_test_proba_RF_ensemble_onlyspatial_features.csv"
RF_ensemble_features_path_only_spatial = "feat_impo_RF_ensemble_onlyspatial_features.npy"
RF_ensemble_coverage_path_only_spatial = "coverage_RF_ensemble_only_spatial.csv"

RF_ensemble_proba_path_only_temporal = "avg_test_proba_RF_ensemble_onlytemporal_features.csv"
RF_ensemble_features_path_only_temporal = "feat_impo_RF_ensemble_onlytemporal_features.npy"
RF_ensemble_coverage_path_only_temporal = "coverage_RF_ensemble_only_temporal.csv"
#%%
def plot_data(data):
    fig = plt.figure(figsize=(5,5))
    ax = fig.add_subplot(111)
    cax = ax.matshow(data) 
    fig.colorbar(cax)
    plt.axis('equal')
    plt.axis('off')
    plt.show()

    #%%
def ROC_curve(pred_class1,y_test, label, color, symbol):
    false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
    roc_auc = auc(false_positive_rate, true_positive_rate)
    plt.plot(false_positive_rate, true_positive_rate, linewidth=2, label= label + 'AUC = %0.3f'% roc_auc, color = color, ls = symbol)
    plt.legend(loc='lower right', fontsize=10)
    plt.xlim([-0.1,1.1])
    plt.ylim([-0.1,1.1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')

    #%%
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def load_data():
    ##%%
    print('---------------------------------')
    print("Reading in data...")
    data = pd.read_csv(data_path,index_col=[1,2]).drop(['Unnamed: 0'],axis=1)  
    print(data.info)
    ##%%

    print(data.shape)
    ##(11123304, 72)
    
    all_columns = data.columns
    print(all_columns)
    
    features = data.drop(['offence','probability','offence_ids','pred_class','probability_norm','pred_class_norm','pred_class_bin','to_date'],axis=1)
    
    target = data['to_date']
    target = target.astype(int)

    print('Features shape: ',features.shape,'Target shape: ', target.shape)
    ##Features shape:  (11123304, 64) Target shape:  (11123304,)
    ##%%
    
    feat_names = features.columns
    
    print('Total Number of cells: ', features.loc['2014-01-14',:].shape[0])
    print('Total Number of days: ', features.loc[(slice('2014-01-14','2017-01-13'),1429),:].shape[0])
    print('Number of training days: ', features.loc[(slice('2014-01-14','2016-01-13'),1429),:].shape[0])
    print('Number of testing days: ', features.loc[(slice('2016-01-14','2017-01-13'),1429),:].shape[0])
    ##Total Number of cells:  10149
    ##Total Number of days:  1096
    ##Number of training days:  730
    ##Number of testing days:  366
    
    print(feat_names)
    print(len(feat_names))
    
    ##%%
    print ('Class distributions - Original dataset of built land-use')
    print('Class 0: ', target.value_counts()[0], '(',float(target.value_counts()[0])/target.shape[0]*100, '% )')
    print('Class 1: ', target.value_counts()[1], '(',float(target.value_counts()[1])/target.shape[0]*100, '% )')
    ##Class 0:  11117038 ( 99.94366781668468 % )
    ##Class 1:  6266 ( 0.0563321833153171 % )
    
    ## split dataset into training and testing data
    X_train = features.loc['2014-01-14':'2016-01-13',:].values
    y_train = target.loc['2014-01-14':'2016-01-13',:].values
    X_test = features.loc['2016-01-14':'2017-01-13',:].values
    y_test = target.loc['2016-01-14':'2017-01-13',:].values

    tot = len(y_train)
    print('Class distributions - Training')
    print('Class 0: ', np.count_nonzero(y_train == 0), '(',float(np.count_nonzero(y_train == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_train == 1), '(',float(np.count_nonzero(y_train == 1))/tot*100, '% )')
    ##Class 0:  7404388 ( 99.94085387992878 % )
    ##Class 1:  4382 ( 0.05914612007121291 % )
    
    tot = len(y_test)
    print('Class distributions - Testing ')
    print('Class 0: ', np.count_nonzero(y_test == 0), '(',float(np.count_nonzero(y_test == 0))/tot*100, '% )')
    print('Class 1: ', np.count_nonzero(y_test == 1), '(',float(np.count_nonzero(y_test == 1))/tot*100, '% )')
    ##Class 0:  3712650 ( 99.94928031349289 % )
    ##Class 1:  1884 ( 0.05071968650710
    
    return X_train,y_train,X_test,y_test,feat_names 
   
def PAI(y_pred,y_test,coverage, days, cells):
		
	# Predictive Accuracy Index (PAI) for every day!
	# one day has 10149 cells
	# the testing set contains 366 days
	#print '...calculating the PAI...'
     

	PAI_list = []
	N_list = []
	n_list = []
	alert_list = []
	nN_list = []
	aA_list = []
	hitrate_list = []
	precision_list = []

	for day in range(days):
         #the predicted probability for that day for each cell
         alert_level_daily = y_pred[(day*cells):((day+1)*cells)]
         #the true label of crime/no crime in that day for each cell
         test = y_test[(day*cells):((day+1)*cells)]
         #how many cells do we want to be predicted as hotspots?
         #cells * coverage, eg. 10149 * 0.05 = 507 alerts
         num_samples = round(len(alert_level_daily)*coverage)
         
         #then take num_samples cells with the highest predicted probability
         index_list = np.argsort(alert_level_daily)[-int(num_samples):]
         
         #predicted hotspots -- all zero initially
         pred = np.zeros((alert_level_daily.shape[0]))
         #set to 1 those who are have the top num_samples probability
         pred[index_list] = 1


         n = float(np.sum((test == pred) & (test == 1))) # number of correctly predicted crime areas within all true crimes
         N = float(np.count_nonzero(test)) # total number of crime areas (!= number of crimes, because target is binary)
         a = float(np.count_nonzero(pred)) # total area predicted as hotspot
         #a is fixed by the coverage area, e.g. for 5% = 507
         A = float(cells) # total area 
         #A is always fixed to number of cells = 10149
         
         tp = 0
         for i in range(cells):
             if ((test[i]==pred[i])and (test[i]==1)):
                 tp+=1
      
         N_list.append(N)
         n_list.append(n)
         alert_list.append(np.count_nonzero(pred))
         aA_list.append(a/A)
         
         if ((N == 0) or (a == 0)):
             PAI_list.append(0)
             nN_list.append(0)
             hitrate_list.append(0)
             precision_list.append(0)
         else:
             PAI_list.append((n/N)/(a/A))
             nN_list.append(n/N) #same as hitrate!
             hitrate_list.append(tp/float(N))  # % correctly predicted true crime areas within all true crimes    
             precision_list.append(tp/float(a))# % correctly predicted tru crime areas within all predictived crimes
         #print(day,tp,hitrate,precision,n, N, a, A)
       
     
	return coverage, np.mean(hitrate_list), np.std(hitrate_list), np.mean(precision_list), np.std(precision_list),  np.mean(PAI_list), np.std(PAI_list) 

#%%
def PAI2(y_pred,y_test,coverage, days, cells):
		
	# Predictive Accuracy Index (PAI) for every day!
	# one day has 10149 cells
	# the testing set contains 366 days
	#print '...calculating the PAI...'
     

	PAI_list = []
	N_list = []
	n_list = []
	alert_list = []
	nN_list = []
	aA_list = []
	hitrate_list = []
	precision_list = []

	for day in range(days):
         #the predicted probability for that day for each cell
         alert_level_daily = y_pred[(day*cells):((day+1)*cells)]
         #the true label of crime/no crime in that day for each cell
         test = y_test[(day*cells):((day+1)*cells)]
         #how many cells do we want to be predicted as hotspots?
         #cells * coverage, eg. 10149 * 0.05 = 507 alerts
         num_samples = round(len(alert_level_daily)*coverage)
         
         #predicted hotspots -- all zero initially
         pred = np.zeros((alert_level_daily.shape[0]))
         
         ###
         sorted_alert_levels = sorted(list(set(alert_level_daily)),reverse=True) 
         
         alert_level_to_no_cells = {}
         for a in sorted_alert_levels:
             alert_level_to_no_cells.setdefault(a, 0)
         for i in range(alert_level_daily.shape[0]):
             alert_level_to_no_cells[alert_level_daily[i]] +=1

         #for a in alert_levels:
         #    alert_level_to_cells.setdefault(a, set())
         #    for i in range(len(alert_level_daily)):
         #        if (alert_level_daily[i]==a):
         #            alert_level_to_cells[a].add(i)

         
         index_list = []
         cummulative_samples = 0
         cummulative_samples_new = 0
         threshold = 0
         
         #for a in sorted_alert_levels:
         #    cummulative_samples_new += cummulative_samples + alert_level_to_no_cells[a]
         #    if ((cummulative_samples<=num_samples) and (num_samples<cummulative_samples_new)):
         #        threshold = a 
         #    cummulative_samples = cummulative_samples_new
                 # everything that is higher belongs to the index
                 # everything that is equal and smaller is not in the index
                 
         for a in sorted_alert_levels:
             if ((cummulative_samples<=num_samples) and (num_samples<cummulative_samples + 1)):
                 threshold = a 
             cummulative_samples = cummulative_samples + alert_level_to_no_cells[a]
                 # everything that is higher belongs to the index
                 # everything that is equal and smaller is not in the index
                        
         for i in range(alert_level_daily.shape[0]): 
                 #try also with equal
                 if (alert_level_daily[i] > threshold):
                     index_list.append(i)
         ###           
         #set to 1 those who are have the top num_samples probability
         pred[index_list] = 1
          
         n = float(np.sum((test == pred) & (test == 1))) # number of correctly predicted crime areas within all true crimes
         N = float(np.count_nonzero(test)) # total number of crime areas (!= number of crimes, because target is binary)
         a = float(np.count_nonzero(pred)) # total area predicted as hotspot
         #a is fixed by the coverage area, e.g. for 5% = 507
         A = float(cells) # total area 
         #A is always fixed to number of cells = 10149
         
         tp = 0
         for i in range(cells):
             if ((test[i]==pred[i])and (test[i]==1)):
                 tp+=1
      
         N_list.append(N)
         n_list.append(n)
         alert_list.append(np.count_nonzero(pred))
         aA_list.append(a/A)
         
         if ((N == 0) or (a == 0)):
             PAI_list.append(0)
             nN_list.append(0)
             hitrate_list.append(0)
             precision_list.append(0)
         else:
             PAI_list.append((n/N)/(a/A))
             nN_list.append(n/N) #same as hitrate!
             hitrate_list.append(tp/float(N))  # % correctly predicted true crime areas within all true crimes    
             precision_list.append(tp/float(a))# % correctly predicted tru crime areas within all predictived crimes
         #print(day,tp,hitrate,precision,n, N, a, A)
       
     
	return np.mean(aA_list), np.mean(hitrate_list), np.std(hitrate_list), np.mean(precision_list), np.std(precision_list),  np.mean(PAI_list), np.std(PAI_list) 
# 
#%%
#def main():
    
    #read in data
    _, _, _, y_test, all_features = load_data()
    
    temporal_features = ['dow', 'holiday', 'event', 'temp', 'hum', 'discomf', 'daylight', 'moon']
    
    prior_features = ['prior1d', 'prior3d', 'prior7d', 'prior14d']

    poi_infra_features = ['buildgs_areafrac', 'buildgs_dens', 'land_swim', 'land_oldtown',
       'land_green', 'land_business', 'land_indust', 'land_central',
       'land_public', 'land_park', 'land_special', 'land_river', 'land_resi1',
       'land_resi2', 'land_resi3', 'land_mix2', 'land_mix3', 'land_no_use',
       'land_dividx', 'poi_infra', 'poi_shop', 'poi_gastro', 'poi_public',
       'poi_edu', 'intersection', 'highway_exit', 'border_cross', 'road_type',
       'pub_trans', 'pub_hous']
       
    demographic_features = [ 'popage_1', 'popage_2',
       'popage_3', 'popage_4', 'popage_dividx', 'popbirth_nonCH', 'popcit_CH',
       'popcit_EU', 'popcit_dividx', 'popcit_europ', 'popcit_noneurop',
       'popdens', 'popmale', 'popstab', 'busi_sec1', 'busi_sec2', 'busi_sec3',
       'busidens', 'busisec_dividx', 'empldens', 'emplmale', 'emplsec_dividx']

    #%%
    print(y_test[y_test>1])  
    print(len(y_test))   
    #3714534 cubes = 366 days * 10149 cells

    #%%
    
    ensemble_probas  = np.load(ensemble_probas_path)
    
    #%%
    print(ensemble_probas.shape)
    no_samples = ensemble_probas.shape[0]
    no_models = ensemble_probas.shape[1]
    no_instances = ensemble_probas.shape[2]

#%%
    all_avg = np.zeros(no_instances)
    for i in range(no_instances):
        all_avg[i] = np.mean(ensemble_probas[:, :, i, 1])
    print(all_avg.shape)
#%%
    stacking_avg = np.zeros(no_instances) 
    for i in range(no_instances):
        sample_avg = np.zeros(no_samples)
        for s in range(no_samples):
            sample_avg[s] = np.mean(ensemble_probas[s, :, i, 1])#over all models in the sample
        stacking_avg[i] = np.mean(sample_avg)
    print(stacking_avg.shape)     
#%%
    ensemble_avg = np.zeros(no_instances) 
    for i in range(no_instances):
        models_avg = np.zeros(no_models)
        for m in range(no_models):
            models_avg[m] = np.mean(ensemble_probas[:, m, i, 1])#over all samples results of that model
        ensemble_avg[i] = np.mean(models_avg)
    print(ensemble_avg.shape)         
    #%%
    all_avg_score = roc_auc_score(y_test, all_avg, average='weighted')
    print ('All averaging AUC score (test set): ', all_avg_score)
    #0.797535840124
    stacking_avg_score = roc_auc_score(y_test, stacking_avg, average='weighted')
    print ('First stack in sample, then over samples averaging AUC score (test set): ', stacking_avg_score)
    #0.797535840124
    ensemble_avg_score = roc_auc_score(y_test, ensemble_avg, average='weighted')
    print ('First average over samples, then stack the models averaging AUC score (test set): ', ensemble_avg_score)
    #0.797535840124
    #so they arent better! we stick to ensembling of ensembling
    
    #TODO: insert here
    #%%
    # read saved probabilities
    avg_test_proba_file_only_temporal = np.loadtxt(RF_ensemble_proba_path_only_temporal, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    final_pred_proba_only_temporal = [row[1] for row in avg_test_proba_file_only_temporal] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    final_score_only_temporal = roc_auc_score(y_test, final_pred_proba_only_temporal, average='weighted')
    print ('RF ensemble only_temporal AUC score (test set): ', final_score_only_temporal)
    #0.539341731544
    #%%
    # read saved probabilities
    avg_test_proba_file_only_spatial = np.loadtxt(RF_ensemble_proba_path_only_spatial, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    final_pred_proba_only_spatial = [row[1] for row in avg_test_proba_file_only_spatial] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    final_score_only_spatial = roc_auc_score(y_test, final_pred_proba_only_spatial, average='weighted')
    print ('RF ensemble only_spatial AUC score (test set): ', final_score_only_spatial)
    #0.788442352498
#%%
    # read saved probabilities
    avg_test_proba_file_only_crime = np.loadtxt(RF_ensemble_proba_path_only_crime, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    final_pred_proba_only_crime = [row[1] for row in avg_test_proba_file_only_crime] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    final_score_only_crime = roc_auc_score(y_test, final_pred_proba_only_crime, average='weighted')
    print ('RF ensemble only_crime AUC score (test set): ', final_score_only_crime)
    #0.59344173724
    #%%
    # read saved probabilities
    stacking_ensemble_proba_file = np.loadtxt(stacking_ensemble_proba_path, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    stacking_ensemble_pred_proba = [row[1] for row in stacking_ensemble_proba_file] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    stacking_ensemble_score = roc_auc_score(y_test, stacking_ensemble_pred_proba, average='weighted')
    print ('Stacking ensemble AUC score (test set): ', stacking_ensemble_score)
    #0.797487934105
    
    # read saved probabilities
    avg_test_proba_file = np.loadtxt(RF_ensemble_proba_path, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    final_pred_proba = [row[1] for row in avg_test_proba_file] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    final_score = roc_auc_score(y_test, final_pred_proba, average='weighted')
    print ('RF ensemble AUC score (test set): ', final_score)
    #0.79824245994
    #%%
    # read saved probabilities
    lr_stack_test_proba_file = np.loadtxt(LR_stacking_proba_path, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    lr_stack_pred_proba = [row[1] for row in lr_stack_test_proba_file] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    lr_stack_score = roc_auc_score(y_test, lr_stack_pred_proba, average='weighted')
    print ('LR stack AUC score (test set): ', lr_stack_score)
    
    
    # read saved probabilities
    RF_undersampling_test_proba_file = np.loadtxt(RF_undersampling_proba_path, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    RF_undersampling_pred_proba = [row[1] for row in RF_undersampling_test_proba_file] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    RF_undersampling_score = roc_auc_score(y_test, RF_undersampling_pred_proba, average='weighted')
    print ('RF undersampling AUC score (test set): ', RF_undersampling_score)
    
    
    # read saved probabilities
    AB_undersampling_test_proba_file = np.loadtxt(AB_undersampling_proba_path, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    AB_undersampling_pred_proba = [row[1] for row in AB_undersampling_test_proba_file] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    AB_undersampling_score = roc_auc_score(y_test, AB_undersampling_pred_proba, average='weighted')
    print ('AB undersampling AUC score (test set): ', AB_undersampling_score)
    
    
    # read saved probabilities
    LR_undersampling_test_proba_file = np.loadtxt(LR_undersampling_proba_path, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    LR_undersampling_pred_proba = [row[1] for row in LR_undersampling_test_proba_file] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    LR_undersampling_score = roc_auc_score(y_test, LR_undersampling_pred_proba, average='weighted')
    print ('LR undersampling AUC score (test set): ', LR_undersampling_score)
    
    
    # read saved probabilities
    LRl1_undersampling_test_proba_file = np.loadtxt(LRl1_undersampling_proba_path, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    LRl1_undersampling_pred_proba = [row[1] for row in LRl1_undersampling_test_proba_file] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    LRl1_undersampling_score = roc_auc_score(y_test, LRl1_undersampling_pred_proba, average='weighted')
    print ('LR l1 undersampling AUC score (test set): ', LRl1_undersampling_score)
    
    #%%
     # read saved probabilities
    RF_heuristic_undersampling_test_proba_file = np.loadtxt(RF_heuristic_undersampling_proba_path, delimiter = ",")
    
    # compute AUC score
    # second column is the positive class!
    RF_heuristic_undersampling_pred_proba = [row[1] for row in RF_heuristic_undersampling_test_proba_file] 
 
    ## roc_auc_score(y_true, y_score, average=’macro’, sample_weight=None)
    ## y_score = Target scores, can either be probability estimates of the positive class, 
    ##     confidence values, or non-thresholded measure of decisions (as returned by “decision_function” on some classifiers).
    RF_heuristic_undersampling_score = roc_auc_score(y_test, RF_heuristic_undersampling_pred_proba, average='weighted')
    print ('RF heuristic undersampling AUC score (test set): ', RF_heuristic_undersampling_score)
    # 0.728841539225
    
    #%%       
    # majority class dummy classifier
    y_test_maj_class = np.zeros_like(y_test)
    print(y_test_maj_class)
    
    final_score_maj_class = roc_auc_score(y_test, y_test_maj_class, average='weighted')
    print ('Majority Class AUC score (test set): ', final_score_maj_class)
    
    #%%
    #stacking by simple average
    test_probas = []

    test_probas.append(RF_undersampling_pred_proba)
    test_probas.append(AB_undersampling_pred_proba)
    test_probas.append(LR_undersampling_pred_proba)
    test_probas.append(LRl1_undersampling_pred_proba)
    
    # compute averages of all models
    avg_stacking_test_proba = np.mean( np.array( test_probas ), axis=0)
    
    score_avg_stacking = roc_auc_score(y_test, avg_stacking_test_proba, average='weighted')
    print ('Average stack AUC score (test set): ', score_avg_stacking)
       
    #%%
    # print ROC curve
    fig, axs = plt.subplots(1, 1)
    
    #ROC_curve(stacking_ensemble_pred_proba, y_test, "stacking ensemble", "black", '-')
    ROC_curve(final_pred_proba, y_test, "RF ensemble ", "black", '-')
    #ROC_curve(avg_stacking_test_proba, y_test, "avg stacking undersampling ", "black", '--')
    #ROC_curve(lr_stack_pred_proba, y_test, "LR stacking undersampling ", "black", '-')
     
    ROC_curve(RF_undersampling_pred_proba, y_test, 'RF undersampling ', 'black', '--')
    
    #ROC_curve(AB_undersampling_pred_proba, y_test, 'AB undersampling ', 'black', ':')
    #ROC_curve(LR_undersampling_pred_proba, y_test, 'LR l2 undersampling ', 'black', '-.')
    #ROC_curve(LRl1_undersampling_pred_proba, y_test, 'LR l1 undersampling ', 'black', ':')
    
    ROC_curve(RF_heuristic_undersampling_pred_proba, y_test, 'RF heuristic undersampling ', 'black', '-.')
   
    #TODO: insert here
    ROC_curve(y_test_maj_class, y_test, 'Majority class ', 'black', ':')
    plt.xlim([0,1])
    plt.ylim([-0,1])
    plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.1)

    #axs.set_aspect('equal', 'box')
    plt.tight_layout()
    plt.yticks(np.arange(0, 1.1, step=0.1))
    plt.xticks(np.arange(0, 1.1, step=0.1))
    plt.show()

#%%
        # print ROC curve
    fig, axs = plt.subplots(1, 1)
    
    ROC_curve(final_pred_proba, y_test, "All ", "black", '-')
    ROC_curve(final_pred_proba_only_crime, y_test, 'Crime only', 'black', ':')
    ROC_curve(final_pred_proba_only_spatial, y_test, 'Spatial only', 'black', ':')
    ROC_curve(final_pred_proba_only_temporal, y_test, 'Temporal only', 'black', ':')
    #TODO: insert here
        
    plt.xlim([0,1])
    plt.ylim([-0,1])
    plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.1)

    #axs.set_aspect('equal', 'box')
    plt.tight_layout()
    plt.yticks(np.arange(0, 1.1, step=0.1))
    plt.xticks(np.arange(0, 1.1, step=0.1))
    plt.show()

    #%%
    #compute coverage area, hitrate, precision, PAI
    #and save it
    
    days = 366
    cells = 10149
    pred_cov_list = np.arange(0.05,1.05,0.05)

    #TODO: insert here
    #%%
    RF_ensemble_cov_list_only_temporal = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(final_pred_proba_only_temporal),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF_ensemble_cov_list_only_temporal.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(RF_ensemble_coverage_path_only_temporal, RF_ensemble_cov_list_only_temporal, delimiter=",")
#coverage:  0.05
#Hitrate:  0.064529903157 0.132884275943
#Precision:  0.000695185436673 0.00121783831142
#PAI:  1.29174356438 2.66004441132    
    #%%
    RF_ensemble_cov_list_only_spatial = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(final_pred_proba_only_spatial),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF_ensemble_cov_list_only_spatial.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(RF_ensemble_coverage_path_only_spatial, RF_ensemble_cov_list_only_spatial, delimiter=",")
#coverage:  0.05
#Hitrate:  0.232534378948 0.225692284877
#Precision:  0.00236039706405 0.00229109395754
#PAI:  4.65481540818 4.51785206945  
    #%%
    RF_ensemble_cov_list_only_crime = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI(np.array(final_pred_proba_only_crime),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF_ensemble_cov_list_only_crime.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(RF_ensemble_coverage_path_only_crime, RF_ensemble_cov_list_only_crime, delimiter=",")

#coverage:  0.05
#Hitrate:  0.160520327426 0.190747355988
#Precision:  0.00170293486813 0.00201284582715
#PAI:  3.21325602179 3.81833316749

    #%% changed to PAI2 to test
    stacking_ensemble_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI2(np.array(stacking_ensemble_pred_proba),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        stacking_ensemble_cov_list.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])

    np.savetxt(stacking_ensemble_coverage_path, stacking_ensemble_cov_list, delimiter=",")

#coverage:  0.05
#Hitrate:  0.255662980765 0.227191780288
#Precision:  0.00259751457734 0.0023628073648
#PAI:  5.11779801142 4.54786859595

#coverage:  0.1
#Hitrate:  0.389630117909 0.251224339062
#Precision:  0.00203235618725 0.00153250216845
#PAI:  3.89591730705 2.51199587896

#coverage:  0.2
#Hitrate:  0.600984060718 0.267899830489
#Precision:  0.00155051279981 0.000998601171551
#PAI:  3.00462425233 1.33936718208    
    #%% changed to PAI2 to test
    RF_ensemble_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI2(np.array(final_pred_proba),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF_ensemble_cov_list.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(RF_ensemble_coverage_path, RF_ensemble_cov_list, delimiter=",")

#coverage:  0.05
#Hitrate:  0.246380285622 0.221092014526
#Precision:  0.00253823519902 0.00229813727291
#PAI:  4.93197932698 4.42576500084

#coverage: 0.1
#Hitrate:  0.402007433975 0.256439841721
#Precision:  0.00207273412474 0.00153811619348
#PAI:  4.01967827331 2.5641457671

#coverage:  0.2
#Hitrate:  0.604166825888 0.264823040751
#Precision:  0.00156262618105 0.00100261760535
#PAI:  3.02053651031 1.32398474905
    #%%
    
    undersampling_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std = PAI(np.array(RF_undersampling_pred_proba),y_test,pred_cov, days, cells)
        
        print('\n')
        print('coverage: ', undersampling_coverage)
        
        print('Hitrate: ', undersampling_hitrate_mean, undersampling_hitrate_std)
        print('Precision: ', undersampling_precision_mean, undersampling_precision_std)
        print('PAI: ', undersampling_PAI_mean, undersampling_PAI_std)
        
        undersampling_cov_list.append([undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std])
  
    np.savetxt(RF_undersampling_coverage_path, undersampling_cov_list,  delimiter=",")
#%%
print(undersampling_cov_list)
#coverage:  0.05
#Hitrate:  0.233024841734 0.214323270828
#Precision:  0.00235500802966 0.00210084578462
#PAI:  4.66463337033 4.29026997166

#coverage:  0.1
#Hitrate:  0.384000440968 0.246245711941
#Precision:  0.00197851893725 0.00146111608562
#PAI:  3.83962608412 2.46221451279

#0.20000000000000001, 
#0.59056340214127101, 0.26699546021114118, 
#0.0015249401060593826, 0.00096607584914059892, 
#2.9525260927742654, 1.3348457761984589
      #%%
    
    AB_undersampling_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std = PAI(np.array(AB_undersampling_pred_proba),y_test,pred_cov, days, cells)
        
        print('\n')
        print('coverage: ', undersampling_coverage)
        
        print('Hitrate: ', undersampling_hitrate_mean, undersampling_hitrate_std)
        print('Precision: ', undersampling_precision_mean, undersampling_precision_std)
        print('PAI: ', undersampling_PAI_mean, undersampling_PAI_std)
        
        AB_undersampling_cov_list.append([undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std])
  
    np.savetxt(AB_undersampling_coverage_path, AB_undersampling_cov_list,  delimiter=",")
#%%
print(AB_undersampling_cov_list)
#coverage:  0.05
#Hitrate:  0.232482249081 0.222359396581
#Precision:  0.00233345189209 0.00221297795459
#PAI:  4.65377188544 4.45113513985

#coverage:  0.1
#Hitrate:  0.377104805486 0.252712068149
#Precision:  0.00193814099976 0.0015046042575
#PAI:  3.77067652303 2.52687170408

#0.20000000000000001, 
#0.58667153656907756, 0.27513068714614236, 
#0.0015303238310587096, 0.0010140223169309954, 
#2.9330686820884564, 1.3755179033725118
#%%    
    LR_undersampling_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std = PAI2(np.array(LR_undersampling_pred_proba),y_test,pred_cov, days, cells)
        
        print('\n')
        print('coverage: ', undersampling_coverage)
        
        print('Hitrate: ', undersampling_hitrate_mean, undersampling_hitrate_std)
        print('Precision: ', undersampling_precision_mean, undersampling_precision_std)
        print('PAI: ', undersampling_PAI_mean, undersampling_PAI_std)
        
        LR_undersampling_cov_list.append([undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std])
  
    np.savetxt(LR_undersampling_coverage_path, LR_undersampling_cov_list,  delimiter=",")
#%%
print(LR_undersampling_cov_list)
#coverage:  0.05
#Hitrate:  0.245901324692 0.236397536464
#Precision:  0.00253284616462 0.00246671828053
#PAI:  4.92239160612 4.73214713526

#coverage:  0.1
#Hitrate:  0.371218944989 0.252841148837
#Precision:  0.00191122237476 0.00145902920656
#PAI:  3.71182371694 2.52816238379  

#0.20000000000000001, 
#0.57333347056707717, 0.27398265577010344, 
#0.0015034052060620743, 0.001007847303945171, 
#2.8663849225543183, 1.3697783120250147  
#%% changed to PAI2 to test   
    LRl1_undersampling_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std = PAI2(np.array(LRl1_undersampling_pred_proba),y_test,pred_cov, days, cells)
        
        print('\n')
        print('coverage: ', undersampling_coverage)
        
        print('Hitrate: ', undersampling_hitrate_mean, undersampling_hitrate_std)
        print('Precision: ', undersampling_precision_mean, undersampling_precision_std)
        print('PAI: ', undersampling_PAI_mean, undersampling_PAI_std)
        
        LRl1_undersampling_cov_list.append([undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std])
  
    np.savetxt(LRl1_undersampling_coverage_path, LRl1_undersampling_cov_list,  delimiter=",")
#%%
print(LRl1_undersampling_cov_list)
#coverage:  0.05
#Hitrate:  0.217960060583 0.216713232933
#Precision:  0.00223106023863 0.00219118429997
#PAI:  4.36307032516 4.33811163913

#coverage:  0.1
#Hitrate:  0.340748970974 0.252829501298
#Precision:  0.00175778621228 0.00142711623459
#PAI:  3.40715399647 2.52804591987

#.20000000000000001, 
#0.54351449628088977, 0.25696047105629355, 
#0.0014064981560741874, 0.00092242649728152299, 
#2.7173047402732755, 1.2846757737686321
    #%%
    print(sum(y_test_maj_class))
    print(sum(y_test))
   
    #%%needed to change to PAI2
    dummy_cov_list = []
    for pred_cov in tqdm(pred_cov_list):
        dummy_coverage, dummy_hitrate_mean, dummy_hitrate_std, dummy_precision_mean, dummy_precision_std, dummy_PAI_mean, dummy_PAI_std = PAI2(np.array(y_test_maj_class),y_test,pred_cov, days, cells)
    
        print('\n')
        print('coverage: ', dummy_coverage)

        print('\n')
        print('DUMMY Hitrate: ', dummy_hitrate_mean, dummy_hitrate_std)
        print('DUMMY Precision: ', dummy_precision_mean, dummy_precision_std)
        print('DUMMY PAI: ', dummy_PAI_mean, dummy_PAI_std)
              
        dummy_cov_list.append([dummy_coverage, dummy_hitrate_mean, dummy_hitrate_std, dummy_precision_mean, dummy_precision_std, dummy_PAI_mean, dummy_PAI_std])
     
    np.savetxt(dummy_coverage_path, dummy_cov_list, delimiter=",")
#%%
print(dummy_cov_list)    
#coverage:  0.05
#DUMMY Hitrate:  0.0627389535176 0.120584424035
#DUMMY Precision:  0.000689796402281 0.00128453600763
#DUMMY PAI:  1.25589277959 2.41382903261

#coverage:  0.1
#DUMMY Hitrate:  0.139315936037 0.178113134304
#DUMMY Precision:  0.000742954049907 0.000954970233326
#DUMMY PAI:  1.39302210329 1.78095586212

#0.20000000000000001, 
#0.24623180780967663, 0.21760747205906175, 
#0.0006487388624189076, 0.00061649528391167368, 
#1.2310377425913341, 1.0879301644962649

#%%
    heuristic_undersampling_cov_list = []
    for pred_cov in tqdm(pred_cov_list):

        undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std = PAI(np.array(RF_heuristic_undersampling_pred_proba),y_test,pred_cov, days, cells)
        
        print('\n')
        print('coverage: ', undersampling_coverage)
        
        print('Hitrate: ', undersampling_hitrate_mean, undersampling_hitrate_std)
        print('Precision: ', undersampling_precision_mean, undersampling_precision_std)
        print('PAI: ', undersampling_PAI_mean, undersampling_PAI_std)
        
        heuristic_undersampling_cov_list.append([undersampling_coverage, undersampling_hitrate_mean, undersampling_hitrate_std, undersampling_precision_mean, undersampling_precision_std, undersampling_PAI_mean, undersampling_PAI_std])
  
    np.savetxt(RF_heuristic_undersampling_coverage_path, heuristic_undersampling_cov_list,  delimiter=",")

#coverage:  0.05
#Hitrate:  0.124627262537 0.170430580486
#Precision:  0.0012772011511 0.00166091578704
#PAI:  2.49475756901 3.41163700464

#coverage:  0.1
#Hitrate:  0.218138479614 0.218870231336
#Precision:  0.00112250666236 0.00114679977166
#PAI:  2.18116988138 2.18848667766

#coverage:  0.2
#Hitrate:  0.431888959553 0.264718034487
#Precision:  0.00113596597486 0.00085699787724
#PAI:  2.15923204458 1.32345976946

    #%% changed to PAI2 to test
    RF_ensemble_cov_list_only_crime = []
    for pred_cov in tqdm(pred_cov_list):

        print('\n')
        print('coverage: ', pred_cov)
        coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std = PAI2(np.array(final_pred_proba),y_test,pred_cov, days, cells)
        
        
        print('Hitrate: ', hitrate_mean, hitrate_std)
        print('Precision: ', precision_mean, precision_std)
        print('PAI: ', PAI_mean, PAI_std)
        
        RF_ensemble_cov_list.append([coverage, hitrate_mean, hitrate_std, precision_mean, precision_std, PAI_mean, PAI_std])
 
    
    np.savetxt(RF_ensemble_coverage_path, RF_ensemble_cov_list, delimiter=",")

#%%
    #RF_ensemble = pd.read_csv(RF_ensemble_coverage_path,header=None)
    #RF_ensemble.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']

    RF_undersampling = pd.read_csv(RF_undersampling_coverage_path,header=None)
    RF_undersampling.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']

    dummy = pd.read_csv(dummy_coverage_path,header=None)
    dummy.columns = ['coverage','hitrate_mean','hitrate_std','precision_mean', 'precision_std', 'PAI_mean', 'PAI_std']

    #%%    
    #print coverage area - hit rate curve
    
    fig, axs = plt.subplots(1, 1)
    #plt.plot(RF_ensemble.coverage, RF_ensemble.hitrate_mean, label='RF ensemble', linewidth=2, color = 'black', ls='-')
    #plt.fill_between(RF_ensemble.coverage, RF_ensemble.hitrate_mean - RF_ensemble.hitrate_std, RF_ensemble.hitrate_mean + RF_ensemble.hitrate_std, color = 'blue', alpha = 0.4)
    plt.plot(RF_undersampling.coverage, RF_undersampling.hitrate_mean, label='RF undersampling', linewidth=2, color = 'black', ls='-.')
    plt.plot(dummy.coverage, dummy.hitrate_mean, label='Majority class', linewidth=2, color = 'black', ls=':')
   
    plt.legend(loc='lower right')
    plt.xlim([0,1])
    plt.ylim([-0,1])
    plt.grid(color='grey', linestyle='-', linewidth=2, alpha=0.1)
    plt.ylabel('Hit Rate')
    plt.xlabel('Coverage Area')
    #axs.set_aspect('equal', 'box')
    plt.tight_layout()
    plt.yticks(np.arange(0, 1.1, step=0.1))
    plt.xticks(np.arange(0, 1.1, step=0.1))
    plt.show()

    print('Done!')
    
    #%%
