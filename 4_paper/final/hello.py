#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 11:08:38 2018

@author: ckadar
"""
import numpy as np
import time
import sys
out_path = "hello_out.txt"
path = "hello.csv"

# better than python hello.py > hello.txt in the command line
# because you flush and can see the progress

def main():
    sys.stdout = open(out_path, 'w')

    print("hello!")
    for i in range(5):    
        print("This prints once 10 sec.")
        sys.stdout.flush()
        time.sleep(10) 
    ar = [1,2,3]
    np.savetxt(path, ar, delimiter=",")
main()