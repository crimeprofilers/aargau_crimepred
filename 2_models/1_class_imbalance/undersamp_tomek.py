import numpy as np

import pandas as pd

import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt

from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import cross_val_score
from sklearn.metrics import roc_auc_score, roc_curve, auc, confusion_matrix, fbeta_score, make_scorer

from imblearn.under_sampling import TomekLinks


def load_data(euler):

	print '...importing data...'
	if euler == 0:
		data = pd.read_csv('/Users/rudimac/Documents/MTEC/THESIS/FEATURES/all_data.csv', index_col=[0,1])
	elif euler == 1:
		data = pd.read_csv('/cluster/home/rudolfm/csv/all_data.csv', index_col=[0,1])
	else:
		print 'Specify data path!'

	print '...splitting into features and target...'
	features = data.drop(['offence','probability','offence_ids','pred_class','probability_norm','pred_class_norm','pred_class_bin'],axis=1)
	target = data['pred_class_bin']
	target = target.astype(int)

	print '\n\nFEATURE SHAPE: ',features.shape,'TARGET SHAPE: ', target.shape

	feat_names = features.columns

	print '\nTotal Number of cells: ', features.loc['2014-01-14',:].shape[0]
	print '\nTotal Number of days: ', features.loc[(slice('2014-01-14','2017-01-13'),1429),:].shape[0]
	print 'Number of training days: ', features.loc[(slice('2014-01-14','2016-01-13'),1429),:].shape[0]
	print 'Number of testing days: ', features.loc[(slice('2016-01-14','2017-01-13'),1429),:].shape[0]

	print '\nClass distributions - Original Dataset'
	print 'Class 0: ', target.value_counts()[0], '(',float(target.value_counts()[0])/target.shape[0]*100, '% )'
	print 'Class 1: ', target.value_counts()[1], '(',float(target.value_counts()[1])/target.shape[0]*100, '% )'

	# split dataset into training and testing data
	X_train = features.loc['2014-01-14':'2016-01-13',:].values
	y_train = target.loc['2014-01-14':'2016-01-13',:].values
	X_test = features.loc['2016-01-14':'2017-01-13',:].values
	y_test = target.loc['2016-01-14':'2017-01-13',:].values

	tot = len(y_train)
	print '\nClass distributions - Training Dataset'
	print 'Class 0: ', np.count_nonzero(y_train == 0), '(',float(np.count_nonzero(y_train == 0))/tot*100, '% )'
	print 'Class 1: ', np.count_nonzero(y_train == 1), '(',float(np.count_nonzero(y_train == 1))/tot*100, '% )'

	tot = len(y_test)
	print '\nClass distributions - Testing Dataset'
	print 'Class 0: ', np.count_nonzero(y_test == 0), '(',float(np.count_nonzero(y_test == 0))/tot*100, '% )'
	print 'Class 1: ', np.count_nonzero(y_test == 1), '(',float(np.count_nonzero(y_test == 1))/tot*100, '% )'

	return X_train,y_train,X_test,y_test,feat_names

def undersamp_near_miss(X_train, y_train):

	print '\n...resampling...'
	tom = TomekLinks(n_jobs=24)
	X_res, y_res = tom.fit_sample(X_train, y_train)
	

	print '\nBalanced Dataset: '
	print 'Class 0: ', np.count_nonzero(y_res == 0)
	print 'Class 1: ', np.count_nonzero(y_res == 1)

	return X_res,y_res

def prob_vote(threshold):

	# The highest probability wins (threshold is used to balance precision and recall)
	print '\n...executing probability vote...'
	y_pred = []

	for sample in y_proba:
		
		if sample[np.argmax(sample)] > threshold:
			y_pred.append(np.argmax(sample))
		else:
			y_pred.append(0)

	# SAVE y_pred
	#np.savetxt("/Users/rudimac/Documents/MTEC/THESIS/MODELS/y_pred.csv", y_pred, delimiter=",")

	return y_pred

def conf_matrix(y_pred,y_test):
	print '\nConfusion Matrix:'
	confusion_abs = pd.DataFrame(data=confusion_matrix(y_test,y_pred),columns=[['class 0','class 1']], index= ['class 0','class 1'])
	confusion_abs['Total'] = pd.Series([np.count_nonzero(y_test == 0),np.count_nonzero(y_test == 1)],index=confusion_abs.index)
	confusion_per = pd.DataFrame(data=np.zeros((2,3)),columns=[['class 0','class 1','Total']], index= ['class 0','class 1'])
	confusion_per.loc['class 0'] = (confusion_abs.loc['class 0']/confusion_abs.loc['class 0'].Total)*100
	confusion_per.loc['class 1'] = (confusion_abs.loc['class 1']/confusion_abs.loc['class 1'].Total)*100

	print '\n ', confusion_per, '\n \n'

def PAI(y_pred,y_test):
		
	# Predictive Accuracy Index (PAI) for every day
	# one day has 10149 cells
	# the testing set contains 366 days
	print '...calculating the PAI...'

	PAI_list = []
	N_list = []
	n_list = []
	alert_list = []
	nN_list = []
	aA_list = []

	for day in range(366):
		pred = y_pred[(day*10149):((day+1)*10149)]
		test = y_test[(day*10149):((day+1)*10149)]

		count = int(0)
		for i in range(len(test)):
			if ( test[i] == 1 and pred[i] == 1 ):
				count += 1
		
		n = float(count) # number of crimes within predictive hotspots
		N = float(np.count_nonzero(test)) # total number of crimes
		a = float(200*200*np.count_nonzero(pred)) # total area of hotspots
		A = float(200*200*10149) # total area
		
		#print n, N, np.count_nonzero(pred)
		N_list.append(N)
		n_list.append(n)
		alert_list.append(np.count_nonzero(pred))
		aA_list.append(a/A)

		if N == 0 or a == 0:
			PAI_list.append(0)
			nN_list.append(0)
		else:
			PAI_list.append((n/N)/(a/A))
			nN_list.append(n/N)
	
	# SAVE PAI_list			
	# np.savetxt("/Users/rudimac/Documents/MTEC/THESIS/MODELS/PAI_list_undersamp.csv", PAI_list, delimiter=",")

	print '\nThe PAI is: ', np.mean(PAI_list),np.std(PAI_list)
	print '--------------'
	print '    Mean number of offences per day: ', np.mean(N_list), np.std(N_list)
	print '    Mean hit rate: ', np.mean(nN_list), np.std(nN_list)
	print '    Mean number of crimes within hotspot: ', np.mean(n_list), np.std(n_list)
	print '    Mean number of alerts per day: ', np.mean(alert_list), np.std(alert_list)
	print '    Mean area percentage: ', np.mean(aA_list), np.std(aA_list)

def ROC_curve(pred_class1,y_test,euler):
	
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
	roc_auc = auc(false_positive_rate, true_positive_rate)

	plt.title('Receiver Operating Characteristic')
	plt.plot(false_positive_rate, true_positive_rate, 'blue', label='v1 AUC = %0.2f'% roc_auc)
	plt.legend(loc='lower right')
	plt.plot([0,1],[0,1],'r--')
	plt.xlim([-0.1,1.1])
	plt.ylim([-0.1,1.1])
	plt.ylabel('True Positive Rate')
	plt.xlabel('False Positive Rate')
	
	if euler == 0:
		plt.savefig('/Users/rudimac/Documents/MTEC/THESIS/MODELS/undersample/ROC_curve_undersamp_tomek.png')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/MODELS/CLASS_IMBALANCE/roc_auc_tomek_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/MODELS/CLASS_IMBALANCE/roc_auc_tomek_ensemble_TPR.csv', true_positive_rate, delimiter=',')
	elif euler == 1:
		plt.savefig('/cluster/home/rudolfm/results/ROC_curve_undersamp_tomek.png')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_tomek_FPR.csv', false_positive_rate, delimiter=',')
		np.savetxt('/cluster/home/rudolfm/results/roc_auc_tomek_ensemble_TPR.csv', true_positive_rate, delimiter=',')

def FEAT_imp(avg_impo, avg_impo_std, feat_names):

	indices = np.argsort(avg_impo)[::-1]
	
	# Print the feature ranking
	print('\nFeature ranking:')
	print '--------------'

	for f in range(len(feat_names)):
		print("%d. Feature: %s (%f)" % (f + 1, feat_names[indices[f]], avg_impo[indices[f]])), 'STD: ', avg_impo_std[indices[f]]

	# Plot the feature importances of the forest
	plt.figure(figsize=(30,8))
	plt.title("Feature importances")
	plt.bar(range(len(feat_names)), avg_impo[indices],color="r", yerr=avg_impo_std[indices], align="center")
	plt.xticks(range(len(feat_names)), feat_names[indices])
	plt.xlim([-1,20])
	plt.show()


if __name__ == '__main__':

	print '\n\n\n\n\n\n'
	
	# local = 0 or cluster = 1:
	euler = 1

	# load data and split into train and test set
	X_train, y_train, X_test, y_test, feat_names = load_data(euler)

	# create a balanced set of the training data by heuristic undersampling
	X_res, y_res = undersamp_near_miss(X_train, y_train)

	# define estimator
	clf = RandomForestClassifier(n_jobs=24)

	# create scoring object
	roc_auc_weighted = make_scorer(roc_auc_score, average='weighted')

	print '\n...training the classifier...'
	
	# fit the model and apply CV
	clf.fit(X_res,y_res)
	CV = cross_val_score(clf, X_res, y_res, cv=5, scoring=roc_auc_weighted)
	print '\nCross validation (ROC AUC): ', CV
	print 'Mean CV Score: ', np.mean(CV), 'Std. Deviation: ', np.std(CV)

	# obtain probability values
	y_proba = clf.predict_proba(X_test)

	# ROC AUC score
	pred_class1 = [row[1] for row in y_proba]
	print 'ROC AUC score (test set): ', roc_auc_score(y_test, pred_class1)

	# Probability vote with threshold
	y_pred = prob_vote(0.5)

	# f1 score
	print '\nF_beta Score (beta = 1): ', fbeta_score(y_test, y_pred, 1)

	# Print Confusion matrix
	conf_matrix(y_pred,y_test)

	# Print PAI
	PAI(y_pred,y_test)

	# Print average feature importance
	impo = clf.feature_importances_
	impo_std = np.std([tree.feature_importances_ for tree in clf.estimators_],axis=0)
	FEAT_imp(impo, impo_std, feat_names)

	# Print ROC curve
	ROC_curve(pred_class1, y_test, euler)


	print '\n-------------------------------------------------------------------------------------------------------------------\n'
	print 'Execution complete'
	print '\n-------------------------------------------------------------------------------------------------------------------\n'

