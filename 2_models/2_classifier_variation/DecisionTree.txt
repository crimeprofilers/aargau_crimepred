Sender: LSF System <lsfadmin@e1200>
Subject: Job 50625805: <python DecisionTree.py> in cluster <euler> Done

Job <python DecisionTree.py> was submitted from host <euler03> by user <rudolfm> in cluster <euler> at Wed Sep 20 09:56:51 2017.
Job was executed on host(s) <24*e1200>, in queue <bigmem.4h>, as user <rudolfm> in cluster <euler> at Wed Sep 20 10:31:57 2017.
</cluster/home/rudolfm> was used as the home directory.
</cluster/home/rudolfm/scripts> was used as the working directory.
Started at Wed Sep 20 10:31:57 2017.
Terminated at Wed Sep 20 10:35:51 2017.
Results reported at Wed Sep 20 10:35:51 2017.

Your job looked like:

------------------------------------------------------------
# LSBATCH: User input
python DecisionTree.py
------------------------------------------------------------

Successfully completed.

Resource usage summary:

    CPU time :                                   251.39 sec.
    Max Memory :                                 20534 MB
    Average Memory :                             11197.88 MB
    Total Requested Memory :                     72000.00 MB
    Delta Memory :                               51466.00 MB
    Max Swap :                                   -
    Max Processes :                              3
    Max Threads :                                4
    Run time :                                   263 sec.
    Turnaround time :                            2340 sec.

The output (if any) follows:

/cluster/apps/python/2.7.13/x86_64/lib64/python2.7/site-packages/sklearn/cross_validation.py:44: DeprecationWarning: This module was deprecated in version 0.18 in favor of the model_selection module into which all the refactored classes and functions are moved. Also note that the interface of the new CV iterators are different from that of this module. This module will be removed in 0.20.
  "This module will be removed in 0.20.", DeprecationWarning)
/cluster/apps/python/2.7.13/x86_64/lib64/python2.7/site-packages/sklearn/grid_search.py:43: DeprecationWarning: This module was deprecated in version 0.18 in favor of the model_selection module into which all the refactored classes and functions are moved. This module will be removed in 0.20.
  DeprecationWarning)
/cluster/home/rudolfm/.local/lib64/python2.7/site-packages/numpy/lib/arraysetops.py:463: FutureWarning: elementwise comparison failed; returning scalar instead, but in the future will perform elementwise comparison
  mask |= (ar1 == a)







...importing data...
...splitting into features and target...


FEATURE SHAPE:  (11123304, 64) TARGET SHAPE:  (11123304,)

Total Number of cells:  10149

Total Number of days:  1096
Number of training days:  730
Number of testing days:  366

Class distributions - Original Dataset
Class 0:  11116466 ( 99.9385254597 % )
Class 1:  6838 ( 0.0614745402985 % )

Class distributions - Training Dataset
Class 0:  7403918 ( 99.9345100469 % )
Class 1:  4852 ( 0.0654899531231 % )

Class distributions - Testing Dataset
Class 0:  3712548 ( 99.9465343432 % )
Class 1:  1986 ( 0.0534656567957 % )

...resampling...

Number of undersampling subsets: ____ 10 ____


Balanced Dataset: 
Class 0:  4852
Class 1:  4852
...hyperparameter tuning...
The best parameters are:  {'splitter': 'random', 'criterion': 'entropy'}

...training the classifier...

Cross validation (ROC AUC):  [ 0.62253086  0.62654321  0.6042268   0.61690722  0.65082474  0.6143299
  0.6171134   0.58226804  0.62329897  0.62453608]
Mean CV: 0.62 (+/- 0.03)
ROC AUC score (test set):  0.751605898788

...executing probability vote...

F_beta Score (beta = 1):  0.00270614137539

Confusion Matrix:

             class 0    class 1  Total
class 0  78.404023  21.595977  100.0
class 1  45.166163  54.833837  100.0 
 

...calculating the PAI...

The PAI is:  2.57303077856 1.2188385393
--------------
    Mean number of offences per day:  5.4262295082 2.91196335694
    Mean hit rate:  0.55375256574 0.25681520025
    Mean number of crimes within hotspot:  2.97540983607 1.88666494467
    Mean number of alerts per day:  2193.57923497 171.89300588
    Mean area percentage:  0.216137475118 0.0169369401793

-------------------------------------------------------------------------------------------------------------------

Execution complete

-------------------------------------------------------------------------------------------------------------------

