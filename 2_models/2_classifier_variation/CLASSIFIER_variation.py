import numpy as np

import pandas as pd

import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, AdaBoostClassifier
from sklearn.cross_validation import cross_val_score
from sklearn.metrics import roc_auc_score, roc_curve, auc, confusion_matrix, fbeta_score, make_scorer
from sklearn.grid_search import GridSearchCV

from imblearn.ensemble import EasyEnsemble


def load_data(euler):

	print '...importing data...'
	if euler == 0:
		data = pd.read_csv('/Users/rudimac/Documents/MTEC/THESIS/FEATURES/dataset_final.csv', index_col=[1,2]).drop(['Unnamed: 0'],axis=1)
	elif euler == 1:
		data = pd.read_csv('/cluster/home/rudolfm/csv/all_data.csv', index_col=[0,1])
	else:
		print 'Specify data path!'

	print '...splitting into features and target...'
	features = data.drop(['offence','probability','offence_ids','pred_class','probability_norm','pred_class_norm','pred_class_bin','to_date'],axis=1)
	target = data['pred_class_bin']
	target = target.astype(int)

	print '\n\nFEATURE SHAPE: ',features.shape,'TARGET SHAPE: ', target.shape

	feat_names = features.columns

	print '\nTotal Number of cells: ', features.loc['2014-01-14',:].shape[0]
	print '\nTotal Number of days: ', features.loc[(slice('2014-01-14','2017-01-13'),1429),:].shape[0]
	print 'Number of training days: ', features.loc[(slice('2014-01-14','2016-01-13'),1429),:].shape[0]
	print 'Number of testing days: ', features.loc[(slice('2016-01-14','2017-01-13'),1429),:].shape[0]

	print '\nClass distributions - Original Dataset'
	print 'Class 0: ', target.value_counts()[0], '(',float(target.value_counts()[0])/target.shape[0]*100, '% )'
	print 'Class 1: ', target.value_counts()[1], '(',float(target.value_counts()[1])/target.shape[0]*100, '% )'

	# split dataset into training and testing data
	X_train = features.loc['2014-01-14':'2016-01-13',:].values
	y_train = target.loc['2014-01-14':'2016-01-13',:].values
	X_test = features.loc['2016-01-14':'2017-01-13',:].values
	y_test = target.loc['2016-01-14':'2017-01-13',:].values

	tot = len(y_train)
	print '\nClass distributions - Training Dataset'
	print 'Class 0: ', np.count_nonzero(y_train == 0), '(',float(np.count_nonzero(y_train == 0))/tot*100, '% )'
	print 'Class 1: ', np.count_nonzero(y_train == 1), '(',float(np.count_nonzero(y_train == 1))/tot*100, '% )'

	tot = len(y_test)
	print '\nClass distributions - Testing Dataset'
	print 'Class 0: ', np.count_nonzero(y_test == 0), '(',float(np.count_nonzero(y_test == 0))/tot*100, '% )'
	print 'Class 1: ', np.count_nonzero(y_test == 1), '(',float(np.count_nonzero(y_test == 1))/tot*100, '% )'

	return X_train,y_train,X_test,y_test,feat_names

def undersamp_easy_ensemble(n_subsets, X_train, y_train):

	print '\n...resampling...'
	# iteratively select a random subset and make an ensemble of the different sets
	print '\nNumber of undersampling subsets: ____', n_subsets, '____\n'
	ee = EasyEnsemble(n_subsets=n_subsets, random_state=4242)
	X_res, y_res = ee.fit_sample(X_train, y_train)

	print '\nBalanced Dataset: '
	print 'Class 0: ', np.count_nonzero(y_res[0] == 0)
	print 'Class 1: ', np.count_nonzero(y_res[0] == 1)

	return X_res,y_res

def ROC_curve_DT(pred_class1,y_test):
	
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
	roc_auc = auc(false_positive_rate, true_positive_rate)

	np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/MODELS/CLASSIFIER_VARIATION/roc_auc_decision_tree_FPR.csv', false_positive_rate, delimiter=',')
	np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/MODELS/CLASSIFIER_VARIATION/roc_auc_decision_tree_TPR.csv', true_positive_rate, delimiter=',')

def ROC_curve_RF(pred_class1,y_test):
	
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
	roc_auc = auc(false_positive_rate, true_positive_rate)

	np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/MODELS/CLASSIFIER_VARIATION/roc_auc_random_forest_FPR.csv', false_positive_rate, delimiter=',')
	np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/MODELS/CLASSIFIER_VARIATION/roc_auc_random_forest_TPR.csv', true_positive_rate, delimiter=',')

def ROC_curve_ET(pred_class1,y_test):
	
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
	roc_auc = auc(false_positive_rate, true_positive_rate)

	np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/MODELS/CLASSIFIER_VARIATION/roc_auc_extra_tree_FPR.csv', false_positive_rate, delimiter=',')
	np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/MODELS/CLASSIFIER_VARIATION/roc_auc_extra_tree_TPR.csv', true_positive_rate, delimiter=',')

def ROC_curve_AB(pred_class1,y_test):
	
	false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, pred_class1)
	roc_auc = auc(false_positive_rate, true_positive_rate)

	np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/MODELS/CLASSIFIER_VARIATION/roc_auc_ada_boost_FPR.csv', false_positive_rate, delimiter=',')
	np.savetxt('/Users/rudimac/Documents/MTEC/THESIS/MODELS/CLASSIFIER_VARIATION/roc_auc_ada_boost_TPR.csv', true_positive_rate, delimiter=',')

def classify_DT(parameters, clf):

	param_grid = parameters

	CV_clf = GridSearchCV(estimator=clf, param_grid=param_grid, cv= 5, scoring='roc_auc')
	
	print '\n...training the classifier...'
	clf_probs = []
	cross_val = []
	importances = []
	importances_std = []

	for i in range(0,n_subsets):
		
		print '    ...hyperparameter tuning (subset: ', i, ')...'
		CV_clf.fit(X_res[i],y_res[i])

		print '        \nThe best score is: ', CV_clf.best_score_
		print '        ( ', CV_clf.best_params_, ')'
		hyperpara = CV_clf.best_params_
		cross_val.append(CV_clf.best_score_)

		# assign optimal hyperparamters to estimator and fit the model
		clf = DecisionTreeClassifier(criterion=hyperpara['criterion'], splitter= hyperpara['splitter'])

		print '        ...making predictions...\n'
		clf.fit(X_res[i],y_res[i])
		clf_probs.append(clf.predict_proba(X_test))
		

	# AVERAGE VALUES
	avg_prob = np.mean( np.array( clf_probs ), axis=0 )
	avg_cv = np.mean( np.array( cross_val ), axis=0 )

	return avg_prob, avg_cv

def classify_RF(parameters, clf):

	param_grid = parameters

	CV_clf = GridSearchCV(estimator=clf, param_grid=param_grid, cv= 5, scoring='roc_auc')
	
	print '\n...training the classifier...'
	clf_probs = []
	cross_val = []
	importances = []
	importances_std = []

	for i in range(0,n_subsets):
		
		print '    ...hyperparameter tuning (subset: ', i, ')...'
		CV_clf.fit(X_res[i],y_res[i])

		print '        \nThe best score is: ', CV_clf.best_score_
		print '        ( ', CV_clf.best_params_, ')'
		hyperpara = CV_clf.best_params_
		cross_val.append(CV_clf.best_score_)

		# assign optimal hyperparamters to estimator and fit the model
		clf = RandomForestClassifier(n_estimators=hyperpara['n_estimators'], max_features=hyperpara['max_features'], n_jobs=-1)

		print '        ...making predictions...\n'
		clf.fit(X_res[i],y_res[i])
		clf_probs.append(clf.predict_proba(X_test))
		

	# AVERAGE VALUES
	avg_prob = np.mean( np.array( clf_probs ), axis=0 )
	avg_cv = np.mean( np.array( cross_val ), axis=0 )

	return avg_prob, avg_cv

def classify_ET(parameters, clf):

	param_grid = parameters

	CV_clf = GridSearchCV(estimator=clf, param_grid=param_grid, cv= 5, scoring='roc_auc')
	
	print '\n...training the classifier...'
	clf_probs = []
	cross_val = []
	importances = []
	importances_std = []

	for i in range(0,n_subsets):
		
		print '    ...hyperparameter tuning (subset: ', i, ')...'
		CV_clf.fit(X_res[i],y_res[i])

		print '        \nThe best score is: ', CV_clf.best_score_
		print '        ( ', CV_clf.best_params_, ')'
		hyperpara = CV_clf.best_params_
		cross_val.append(CV_clf.best_score_)

		# assign optimal hyperparamters to estimator and fit the model
		clf = ExtraTreesClassifier(n_estimators=hyperpara['n_estimators'], max_features=hyperpara['max_features'], n_jobs=-1)

		print '        ...making predictions...\n'
		clf.fit(X_res[i],y_res[i])
		clf_probs.append(clf.predict_proba(X_test))
		

	# AVERAGE VALUES
	avg_prob = np.mean( np.array( clf_probs ), axis=0 )
	avg_cv = np.mean( np.array( cross_val ), axis=0 )

	return avg_prob, avg_cv

def classify_AB(parameters, clf):

	param_grid = parameters

	CV_clf = GridSearchCV(estimator=clf, param_grid=param_grid, cv= 5, scoring='roc_auc')
	
	print '\n...training the classifier...'
	clf_probs = []
	cross_val = []
	importances = []
	importances_std = []

	for i in range(0,n_subsets):
		
		print '    ...hyperparameter tuning (subset: ', i, ')...'
		CV_clf.fit(X_res[i],y_res[i])

		print '        \nThe best score is: ', CV_clf.best_score_
		print '        ( ', CV_clf.best_params_, ')'
		hyperpara = CV_clf.best_params_
		cross_val.append(CV_clf.best_score_)

		# assign optimal hyperparamters to estimator and fit the model
		clf = AdaBoostClassifier(DecisionTreeClassifier(criterion=hyperpara['base_estimator__criterion'], splitter= hyperpara['base_estimator__splitter']),
		n_estimators=int(hyperpara['n_estimators']),learning_rate=float(hyperpara['learning_rate']))

		print '        ...making predictions...\n'
		clf.fit(X_res[i],y_res[i])
		clf_probs.append(clf.predict_proba(X_test))
		

	# AVERAGE VALUES
	avg_prob = np.mean( np.array( clf_probs ), axis=0 )
	avg_cv = np.mean( np.array( cross_val ), axis=0 )

	return avg_prob, avg_cv

if __name__ == '__main__':

	print '\n\n\n\n\n\n'

	# load data and split into train and test set
	X_train, y_train, X_test, y_test, feat_names = load_data()

	# create a number of balanced subsets of the training data by undersampling
	n_subsets = 10
	X_res, y_res = undersamp_easy_ensemble(n_subsets, X_train, y_train)

	print '\n\n\n---------------------------------------------------------------------------------------------------------------\n\n\n'
	# define estimator
	dt = DecisionTreeClassifier()

	# define grid containing a set of hyperparameters
	param_DT = { 
    'criterion' : ['gini', 'entropy'],
    'splitter' : ['best', 'random'],
	}

	# train models and obtain averaged probability scores
	DT_prob, DT_cv = classify_DT(param_DT, dt)

	# CROSS VALIDATION - ROC AUC SCORE
	print '\nCross validation (ROC AUC): ', DT_cv

	# ROC AUC score
	pred_class1 = [row[1] for row in DT_prob]
	print '     \nROC AUC score (test set): ', roc_auc_score(y_test, pred_class1, average='weighted')
	ROC_curve_DT(pred_class1,y_test)


	print '\n\n\n---------------------------------------------------------------------------------------------------------------\n\n\n'
	# define estimator
	rf = RandomForestClassifier(n_jobs=-1)

	# define grid containing a set of hyperparameters
	param_RF = { 
    'n_estimators': [700, 1000, 1500],
    'max_features': ['auto', 'sqrt', 'log2']
	}

	# train models and obtain averaged probability scores
	RF_prob, RF_cv = classify_RF(param_RF, rf)

	# CROSS VALIDATION - ROC AUC SCORE
	print '\nCross validation (ROC AUC): ', RF_cv

	# ROC AUC score
	pred_class1 = [row[1] for row in RF_prob]
	print '     \nROC AUC score (test set): ', roc_auc_score(y_test, pred_class1, average='weighted')
	ROC_curve_RF(pred_class1,y_test)
	

	print '\n\n\n---------------------------------------------------------------------------------------------------------------\n\n\n'
	# define estimator
	et = ExtraTreesClassifier(n_jobs=-1)

	# define grid containing a set of hyperparameters
	param_ET = { 
    'n_estimators': [700, 1000, 1500],
    'max_features': ['auto', 'sqrt', 'log2']
	}

	# train models and obtain averaged probability scores
	ET_prob, ET_cv = classify_ET(param_ET, et)

	# CROSS VALIDATION - ROC AUC SCORE
	print '\nCross validation (ROC AUC): ', ET_cv

	# ROC AUC score
	pred_class1 = [row[1] for row in ET_prob]
	print '     \nROC AUC score (test set): ', roc_auc_score(y_test, pred_class1, average='weighted')
	ROC_curve_ET(pred_class1,y_test)


	print '\n\n\n---------------------------------------------------------------------------------------------------------------\n\n\n'
	# define estimator
	ab = AdaBoostClassifier(DecisionTreeClassifier())

	# define grid containing a set of hyperparameters
	param_AB = { 
    'n_estimators' : [400, 700, 1000],
    'learning_rate': [0.5, 1.0, 2.0],
    'base_estimator__criterion' : ['gini', 'entropy'],
    'base_estimator__splitter' : ['best', 'random']
	}

	# train models and obtain averaged probability scores
	AB_prob, AB_cv = classify_AB(param_AB, ab)

	# CROSS VALIDATION - ROC AUC SCORE
	print '\nCross validation (ROC AUC): ', AB_cv

	# ROC AUC score
	pred_class1 = [row[1] for row in AB_prob]
	print '     \nROC AUC score (test set): ', roc_auc_score(y_test, pred_class1, average='weighted')
	ROC_curve_AB(pred_class1,y_test)

	print '\n-------------------------------------------------------------------------------------------------------------------\n'
	print 'Execution complete'
	print '\n-------------------------------------------------------------------------------------------------------------------\n'

