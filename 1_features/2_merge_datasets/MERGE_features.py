# This script merges the static, semi-static and temporal feature sets into one large array including
# features and target variables.
#
#
# Rudolf Maculan
# ETH Zurich
# 20-07-2017

import pandas as pd

# 
# SET PATH TO CSV DATA HERE:
path = '/Users/rudimac/Documents/MTEC/THESIS/git/Aargau_Feature_Data'

print 'loading csv files...'
static_feat = pd.read_csv(path + '/static_feat.csv').drop(['Unnamed: 0'],axis=1)
census = pd.read_csv(path + '/semi_static_feat.csv').drop(['Unnamed: 0'],axis=1)
temp_feat = pd.read_csv(path + '/temp_feat.csv').drop(['Unnamed: 0'],axis=1)

print static_feat.columns
print census.columns
print temp_feat.columns

# JOIN ALL FEATURES
print '...merging dataframes step 1...'

features_raw = pd.merge(temp_feat,census,how='left',on=['cen_year','grid_id'])

print features_raw.shape

print '...merging dataframes step 2...'
features_raw = pd.merge(features_raw,static_feat,how='left',on=['grid_id'])

print features_raw.shape

print features_raw.columns

# Filter out all cells with land_no_use = 100%
features_raw = features_raw[features_raw['land_no_use']!=1]

print '...writing csv file...'
features_raw.drop(['cen_year'],axis=1).to_csv(path + '/ALL_features.csv')

