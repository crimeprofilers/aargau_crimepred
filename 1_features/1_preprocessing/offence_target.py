# This script creates the offence features from the offence dataset provided by the Kantonspolizei Aargau.

# Rudolf Maculan
# ETH Zurich
# 14-07-2017


import numpy as np
import pandas as pd 
from datetime import timedelta
from tqdm import tqdm


print '...loading csv...'
feature_frame = pd.read_csv("/Users/rudimac/Documents/MTEC/THESIS/PROCESSING/temporal_features_frame.csv").drop(['Unnamed: 0'],axis=1)

pol_data = pd.read_csv("/Users/rudimac/Documents/MTEC/THESIS/Datasets_RAW/offences/offences_ag_gridded.csv")
pol_data.grid_id = pol_data.grid_id.astype(int) 

print '...building multiindex dataframe...'

tuples = [tuple(x) for x in feature_frame.values]
idx = pd.MultiIndex.from_tuples(tuples, names=['date', 'grid_id'])

offences = pd.DataFrame(np.zeros((feature_frame.shape[0],2)),index=idx,columns=['offence','probability'])
offences['offence'] = offences['offence'].astype(int)
offences['offence_ids'] = np.empty((len(offences), 0)).tolist()
offences = offences.sort_index()


grid_values = feature_frame['grid_id'].unique()

print '...starting the loop...'

key_err = int(0)
attr_err = int(0)

for row in tqdm(pol_data.itertuples()):
	
	if row[14] in grid_values:
		
		try:
			if pd.to_datetime(row[7]) >= pd.to_datetime('2013-12-31') and pd.to_datetime(row[10]) < pd.to_datetime('2017-02-22'):

				nmb_days = (pd.to_datetime(row[10]) - pd.to_datetime(row[7])).days+1
				day_range = pd.DataFrame(pd.date_range(row[7],periods=nmb_days,freq='D'))

				for day in day_range.itertuples():

					offences.loc[(str(day[1].date()),row[14]),'offence'] += 1
					offences.loc[(str(day[1].date()),row[14]),'probability'] = offences.loc[(str(day[1].date()),row[14]),'probability'] + float(1)/nmb_days
					offences.loc[(str(day[1].date()),row[14]),'offence_ids'].append(row[1])

		except AttributeError:
			attr_err += 1
			print 'attribute error'
			offences.loc[(str(row[10].date()),row[14]),'offence'] += 1
			offences.loc[(str(row[10].date()),row[14]),'probability'] = offences.loc[(str(row[10].date()),row[14]),'probability'] + float(1)/nmb_days
			offences.loc[(str(row[10].date()),row[14]),'offence_ids'].append(row[1])
	else:
		key_err += 1

print 'Number of offences outside grid: ', key_err
print 'Number of missing "from_date" values', attr_err

#print offences[(offences['offence']>0)]

print '...copying data to csv file...'
offences.to_csv("/Users/rudimac/Documents/MTEC/THESIS/FEATURES/target/offences.csv")

