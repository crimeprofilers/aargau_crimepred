# This script creates the events feature in the necessary structure.
# QGIS was used to map the event points to grid cells. 

# Rudolf Maculan
# ETH Zurich
# 10-07-2017

import numpy as np
import pandas as pd 
from tqdm import tqdm


print '...importing data...'
events_raw = pd.read_csv('/Users/rudimac/Documents/MTEC/THESIS/QGIS/csv/final/events_no_buffer.csv')
events = pd.read_csv('/Users/rudimac/Documents/MTEC/THESIS/PROCESSING/temporal_features_frame.csv').drop(['Unnamed: 0'],axis=1)

events['event'] = np.zeros(events.shape[0],dtype='int8')


print 'total number of iterations: ', events_raw.shape[0]

for row in tqdm(events_raw.itertuples()):
    #print row
    events['event'].loc[(events['grid_id'] == row[1])&(events['date'] == row[4])] += 1

print '...writing data to csv...'
events.to_csv('/Users/rudimac/Documents/MTEC/THESIS/FEATURES/events/events_no_buffer.csv')
