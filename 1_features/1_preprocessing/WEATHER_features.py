# With this script the feature array for the weather data is constructed from the dataset created by web scraping.
#
#
# Rudolf Maculan
# ETH Zurich
# 09-06-2017

import numpy as np
import pandas as pd # DataFrame for doing computations efficently
from datetime import timedelta

print 'reading csv...'

# import table that links the grid to the weather zones
link_table = pd.read_csv("/Users/rudimac/Documents/MTEC/THESIS/QGIS/csv/final/weather_gridded.csv").drop(
['xmin','xmax','ymin','ymax','xmin_1','xmax_1','ymin_1','ymax_1'],axis=1)
link_table.columns = ['grid_id','weather_zone']

# import weather data
weather_data = pd.read_csv("/Users/rudimac/Documents/MTEC/THESIS/PROCESSING/weather_zone_data.csv")
weather_data.rename(columns={'weather_id':'weather_zone'}, inplace=True)


# convert format of date and daylight column
for i in weather_data.itertuples():
    weather_data.set_value(i.Index,'date',i.date[:10])
    weather_data.set_value(i.Index,'daylight',(pd.to_datetime(weather_data.loc[i.Index].sunset)-
         pd.to_datetime(weather_data.loc[i.Index].sunrise)).total_seconds()/3600)
    
    f.value +=1


# In order to generate the MultIndex DataFrame we import the temporal features frame file 
# generated earlier
feature_frame = pd.read_csv("/Users/rudimac/Documents/MTEC/THESIS/PROCESSING/temporal_features_frame.csv").drop(['Unnamed: 0'],axis=1)

print 'constucting DataFrame...'

# create MultIndex DataFrame
shard = feature_frame
tuples = [tuple(x) for x in shard.values]

# create multindex object from tuples
idx = pd.MultiIndex.from_tuples(tuples, names=['date', 'grid_id'])

weather_features = pd.DataFrame(np.zeros((28034700,7)),index=idx,
                                columns=['weather_zone','temp','hum','discomf','cloud',
                                         'daylight','moon'])


# construct features 
for day, day_df in weather_features[:24378].groupby(level=0):
    
    print day

    for field in day_df.itertuples():

        #weatherzone
        zone = link_table.loc[link_table['grid_id'] == field[0][1]].weather_zone
        weather_features.set_value(field[0],'weather_zone',zone)
        
        #temp
        weather_features.set_value(field[0],'temp', (weather_data[(weather_data['date'] == 
                        field[0][0]) & (weather_data['weather_zone'] == int(zone))].temp))
        
        #hum
        weather_features.set_value(field[0],'hum', (weather_data[(weather_data['date'] == 
                        field[0][0]) & (weather_data['weather_zone'] == int(zone))].hum))
        
        #discomf
        weather_features.set_value(field[0],'discomf', (weather_data[(weather_data['date'] == 
                        field[0][0]) & (weather_data['weather_zone'] == int(zone))].discomf))
        
        #cloud
        weather_features.set_value(field[0],'cloud', (weather_data[(weather_data['date'] == 
                        field[0][0]) & (weather_data['weather_zone'] == int(zone))].cloud))
        
        #daylight
        weather_features.set_value(field[0],'daylight', (weather_data[(weather_data['date'] == 
                        field[0][0]) & (weather_data['weather_zone'] == int(zone))].daylight))
        
        #moon
        weather_features.set_value(field[0],'moon', (weather_data[(weather_data['date'] == 
                        field[0][0]) & (weather_data['weather_zone'] == int(zone))].moon))




#convert weather_zone to int
weather_features['weather_zone'] = weather_features['weather_zone'].astype(int)


# write to csv
weather_features.to_csv("/Users/rudimac/Documents/MTEC/THESIS/FEATURES/weather/weather_features.csv")

