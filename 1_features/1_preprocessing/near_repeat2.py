# This script assigns the near repeat victimization features based on the offense dataset.
#
# Rudolf Maculan
# ETH Zurich
# 13-07-2017

import numpy as np
import pandas as pd 
from datetime import timedelta
from tqdm import tqdm


print '...reading input csv files...'

# offence dataset
pol_data = pd.read_csv("/Users/rudimac/Documents/MTEC/THESIS/Datasets_RAW/offences/offences_ag_gridded.csv")

# import neigboring cells dataset
neighbours = pd.read_csv("/Users/rudimac/Documents/MTEC/THESIS/QGIS/csv/NEIGHBOURS_raw.csv").drop(
['xmin','xmax','ymin','ymax','xmin_2','xmax_2','ymin_2','ymax_2'],axis=1)
neighbours.columns = ['grid_id','neighbour_id']
neighbours = neighbours.set_index(['grid_id'])

# In order to generate the MultIndex DataFrame we import the temporal features frame file 
# generated earlier
feature_frame = pd.read_csv("/Users/rudimac/Documents/MTEC/THESIS/PROCESSING/temporal_features_frame.csv").drop(['Unnamed: 0'],axis=1)


print '...building multiindex dataframe...'

tuples = [tuple(x) for x in feature_frame.values]
idx = pd.MultiIndex.from_tuples(tuples, names=['date', 'grid_id'])

near_repeat = pd.DataFrame(np.zeros((feature_frame.shape[0],4),dtype='int8'),index=idx,columns=['prior1d','prior3d','prior7d','prior14d'])
near_repeat = near_repeat.sort_index()


grid_values = feature_frame['grid_id'].unique()

print '...starting the loop...'

for row in tqdm(pol_data.itertuples()):

	if row[14] in grid_values and pd.to_datetime(row[10]) <= (pd.to_datetime('2017-02-22')-timedelta(days=14)):

	    subset = neighbours.loc[row[14]]
	    
	    for cell in subset.itertuples():

	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=1))[:10],cell[1]),'prior1d'] += 1

	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=1))[:10],cell[1]),'prior3d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=2))[:10],cell[1]),'prior3d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=3))[:10],cell[1]),'prior3d'] += 1

	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=1))[:10],cell[1]),'prior7d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=2))[:10],cell[1]),'prior7d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=3))[:10],cell[1]),'prior7d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=4))[:10],cell[1]),'prior7d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=5))[:10],cell[1]),'prior7d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=6))[:10],cell[1]),'prior7d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=7))[:10],cell[1]),'prior7d'] += 1

	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=1))[:10],cell[1]),'prior14d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=2))[:10],cell[1]),'prior14d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=3))[:10],cell[1]),'prior14d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=4))[:10],cell[1]),'prior14d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=5))[:10],cell[1]),'prior14d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=6))[:10],cell[1]),'prior14d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=7))[:10],cell[1]),'prior14d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=8))[:10],cell[1]),'prior14d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=9))[:10],cell[1]),'prior14d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=10))[:10],cell[1]),'prior14d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=11))[:10],cell[1]),'prior14d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=12))[:10],cell[1]),'prior14d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=13))[:10],cell[1]),'prior14d'] += 1
	        near_repeat.loc[(str(pd.to_datetime(row[10])+timedelta(days=14))[:10],cell[1]),'prior14d'] += 1

print '...copying to csv file...'

near_repeat.to_csv('/Users/rudimac/Documents/MTEC/THESIS/FEATURES/near_repeat/near_repeat2.csv')
