# This script creates a csv file containing all events in Switzerland, starting from a specific date.
# The events are scraped from the website "www.events.ch". According to the website: "The Swiss Event Database (SEDB) 
# is behind events.ch, which is Switzerland’s largest event database and currently lists around 30,000 events. 
# The editorial staff cover parties, live concerts, theaters, operas, musicals and circuses, exhibitions in 
# countless museums and galleries, artists and DJs."
#

# Rudolf Maculan
# ETH Zurich
# 23-03-2017

import urllib
import datetime
import progressbar
import time

from progressbar import ProgressBar
pbar = ProgressBar()

# write values into .txt file
def print_to_txt(event_list):
	text_file = open('events_2017.txt','w')
	n = 1
	for item in event_list:
		if n%5 != 0:
			text_file.write(item)
			text_file.write("; ")
		else:
			text_file.write(item)
			text_file.write("; \n")
		n = n+1
	text_file.close()

def main():

	### PLEASE DEFINE START TIME : YYYY,MM,DD ###
	start_date = datetime.datetime(2016,12,31)

	#initialize list
	event_list = []
	event_list.append("Event_ID")
	event_list.append("NAME")
	event_list.append("TIME")
	event_list.append("LOCATION")
	event_list.append("CATEGORY")

	error_count = 0

	#loop through the specified time range
	for page_number in pbar(range(0, 10000)):

		#specify url
		event = "http://events.ch/de/Suche/" + str(start_date.year) + "-" + str(start_date.month) + "-" + str(start_date.day) + "/s/" + str(page_number)

		#Query the website and return the html to the variable 'page'
		try:
			page = urllib.urlopen(event)
		except HTTPError:
			time.sleep(20)
			page = urllib.urlopen(event)

		#import the Beautifulsoup functions to parse the data returned from the website
		from bs4 import BeautifulSoup

		#Parse the html in the 'page' variable, and store it in Beautiful Soup format
		soup = BeautifulSoup(page,"lxml")

		#extract event title and write to list
		all_events = soup.find_all("li", class_= "event")
		for event_info in all_events:
			try:
				if event_info.get("data-id") != None and event_info.div.ul.li.ul.li.a != None:
					event_list.append(event_info.get("data-id"))
					event_list.append(str(event_info.div.h2.a.get("title").encode('utf-8')))
					event_list.append(str(event_info.div.time.get("datetime")))
					event_list.append(str(event_info.ul.li.ul.li.a.text.encode('utf-8')))
					event_list.append(str(event_info.a.get("href").encode('utf-8')))
			except AttributeError:
				error_count = error_count + 1
		
		print_to_txt(event_list)
		print page_number, "/10000"

	list_length = len(event_list)/4-1
	print "================================================="
	print "You have scraped ", list_length, "events!"
	print "================================================="
	print "The data has been written to the  file events.txt"
	print "================================================="


if __name__ == "__main__":
	main()