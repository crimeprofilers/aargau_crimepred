# This program provides historical weather data for a number of locations specified by the csv file "weather_grid.csv". 

# The data is extracted from the darksky API.
# The input file is a .csv file which contains the locations and the repective date and time.
# The program generates a .txt file containing location, event_id, time and the respective weather data.

# Rudolf Maculan
# ETH Zurich
# last edit: 03-05-2017

import forecastio # The DARKSKY API is the source of the weather data
import datetime
from datetime import timedelta, date
import csv
import numpy as np

def print_to_txt(data_list): # This function writes values into a .txt file with predefined name
	text_file = open('weather_output.txt','w')
	n = 1
	for item in data_list:
		if n%11 != 0:
			text_file.write(item)
		else:
			text_file.write(item + '\n')
		n = n+1
	text_file.close()
	print "======The data has been written to a .txt file==========" + '\n'


def main():
	
	# To use the API we need an account and the respective API key
	api_key = "25be7ef25d90aa767e3ecf0228e53350"
	unit = "si"

	# Initialize list (including header) in which the data is stored
	data_list = []
	data_list.append('Lat; ')
	data_list.append('Long; ')
	data_list.append('Event_ID; ')
	data_list.append('Time; ')
	data_list.append('Summary; ')
	data_list.append('Temperature; ')
	data_list.append('Humidity; ')
	data_list.append('CloudCover; ')
	data_list.append('sunriseTime; ')
	data_list.append('sunsetTime; ')
	data_list.append('moonPhase; ')


	# read from .csv file and write into data_list array
	with open('weather_points.csv','rb') as csvfile:
		point_list = csv.reader(csvfile, delimiter = ',')
		next(point_list, None)  # skip the header
		
		count = 0 # counts iterations of API calls to restrict the calls (1000 calls per day are free)

		for row in point_list:
			
			# read data from the input file (for each event):
			lat = row[1]
			lng = row[0]
			date_raw = row[8]
			year = '20' + date_raw[-2:]
			month = date_raw[3:-3]
			day = date_raw[:2]
			time_raw = row[9]
			hour = time_raw[:2]
			if hour == '':
				hour = 12 # Some entries (88 out of 7418) do not have a specified time. For these events the time is set to noon.

			# convert to integers:
			lat = float(lat)
			lng = float(lng)
			year = int(year)
			month = int(month)
			day = int(day)
			hour = int(hour)

			time = datetime.datetime(year,month,day,hour) 
						
			data_list.append(row[0] + '; ')
			data_list.append(row[1] + '; ')
			data_list.append(row[2] + '; ')
			data_list.append(str(time) + '; ')

			# Run load_forecast() with the given lat, lng, and time arguments.
			forecast = forecastio.load_forecast(api_key, lat, lng, time=time, units=unit)
			weather_hourly = forecast.hourly()
			weather_daily = forecast.daily()

			for hourlyData in weather_hourly.data[:1]:
				data_list.append(str(hourlyData.summary) + '; ')
				data_list.append(str(hourlyData.temperature) + '; ')
				data_list.append(str(hourlyData.humidity) + '; ')

			for dailyData in weather_daily.data[:1]:
				try: 
					data_list.append(str(dailyData.cloudCover) + '; ')
				except:
					data_list.append('n.a.; ')
				data_list.append(str(dailyData.sunriseTime) + '; ')
				data_list.append(str(dailyData.sunsetTime) + '; ')
				data_list.append(str(dailyData.moonPhase) + '; ')

			count = count + 1
			print count

			if count > 7500: break # specify the limit of API calls 

	print_to_txt(data_list) # write the data to the .txt file

if __name__ == "__main__":
	main()
