# This script creates a dataset for holidays in the canton of Aargau by means of automatic HTML parsing of the 
# website "www.feiertagskalender.ch"

# Rudolf Maculan
# ETH Zurich
# 06-06-2017


import urllib
import datetime
import progressbar
import time

from progressbar import ProgressBar
pbar = ProgressBar()

# write values into .txt file
def print_to_txt(holiday_list):
	text_file = open('holidays_raw.txt','w')
	n = 1
	for item in holiday_list:
		if n%7 != 0:
			text_file.write(item)
			text_file.write("; ")
		else:
			text_file.write(item)
			text_file.write("; \n")
		n = n+1
	text_file.close()


def main():

	#initialize list
	holiday_list = []
	holiday_list.append("Municipality")
	holiday_list.append("Start")
	holiday_list.append("-")
	holiday_list.append("End")
	holiday_list.append("Type")
	holiday_list.append(" ")
	holiday_list.append(" ")


	error_count = 0


	#loop through the years 2014-2017
	for year in pbar(range(2014,2017)):

		geo = 3
		#loop through all regions
		for page_number in range(0, 242):

			#specify url
			holiday = "https://www.feiertagskalender.ch/ferien.php?geo=" + str(geo) + "&jahr=" + str(year)

			#query the website and return the html to the variable 'page'
			try:
				page = urllib.urlopen(holiday)
			except HTTPError:
				time.sleep(20)
				page = urllib.urlopen(holiday)

			#import the Beautifulsoup functions to parse the data returned from the website
			from bs4 import BeautifulSoup

			#Parse the html in the 'page' variable, and store it in Beautiful Soup format
			soup = BeautifulSoup(page,"lxml")
			
			for row in soup.findAll('tr'):
				holiday_list.append(str(soup.title.text.encode('utf-8')))
				
				columns = row.findAll('th')
				for column in columns:
					holiday_list.append(str(0))
					holiday_list.append(str(0))

				columns = row.findAll('td')
				for column in columns:
					
					try:
						holiday_list.append(str(column.text.encode('utf-8')))

					except AttributeError:
						error_count = error_count + 1

			print_to_txt(holiday_list)

			geo += 1 
	
	print 'Errors:', error_count

	list_length = len(event_list)/4-1
	print "================================================="
	print "You have scraped ", list_length, "events!"
	print "================================================="
	print "The data has been written to the  file events.txt"
	print "================================================="


if __name__ == "__main__":
	main()