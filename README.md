# README #


### What is this repository for? ###

	This repository contains all data and scripts necessary to recompute the results presented in the master thesis "Spatio-temporal Crime Prediction for Switzerland" at the Mobiliar Analytics Lab at ETH Zurich.

	The process contains four general tasks:
		Task 0: Initial Analysis.
		Task 1: Create feature array and analyze features.
		Task 2: Run machine learning models.
		Task 3: Run hotspot baseline implementation.

	Task 2 can be skipped by directly downloading and unpacking the zip file "all_data.csv" from the following link:
		
		https://polybox.ethz.ch/index.php/s/q75DNrcKhOB1Ww1


### How do I get set up? ###


TASK 1:
-----------------------------------------------------------------------------------------------------------------

TASK 1 can be subdivided into the following 4 subtasks

	Subtask 0: Data Collection

		This folder contains the scripts used for creating the datasets on events, holidays and weather by means of automated HTML parsing and API calls.

	Subtask 1: Preprocessing

		This folder contains all scripts used for preprocessing the various datasets.

	Subtask 3: Merge Datasets

		In order to generate one large features array follow these 4 steps:

			Step 1.) Download the csv files using the following link: https://polybox.ethz.ch/index.php/s/WkapsfMGGKADE7D

			Step 2.) Unzip the files and save on local disk

			Step 3.) Modify path in the iPython Notebook "ALL_features.ipynb" and run the notebook

			Step 4.) Modify the path in the "MERGE_features.py" script and run the script

	Subtask 4: Postprocessing

		This folder contains scripts for calculating correlations and descriptive statistics and generating plots.


TASK 2:
-----------------------------------------------------------------------------------------------------------------

TASK 2 contains the following 3 subtasks:

	Subtask 1: Class Imbalance

		This folder contains all scripts within the context of class imbalance. These scripts should be executed on a computing cluster. The output files from the cluster are contained in the folder.

	Subtask 2: Classifier Variation

		This folder contains scripts which evaluate the following machine learning models: DecisionTree, RandomForest, ExtraTree and AdaBoost. These scripts should be executed on a computing cluster. The output files from the cluster are contained in the folder.

	Subtask 3: Feature Selection

		This folder contains two scripts and their respective output files. The script "no_autocorrelation.py" recalculates the RandomForest model after eliminating all features that have correlation values with each other above 0.8. The second script recalculates the model after eliminating all temporal features.


TASK 3
-----------------------------------------------------------------------------------------------------------------

TASK 3 is the implementation of a prospective hotspot model that acts as baseline for this project. The model is built upon code provided by the University of Leeds which can be downloaded here: 

	https://github.com/QuantCrimAtLeeds/PredictCode

In order for the baseline implementation to run, one has use Python 3. Apart from the prospective hotspot script the folder contains an implementation of the Wilcoxon signed rank statistical test which is perfomed to evaluate the statisical significance between the machine learning model and the baseline.




### Who do I talk to? ###

Rudolf Maculan
rudolfm@student.ethz.ch